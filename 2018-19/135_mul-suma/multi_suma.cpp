#include <stdio.h>
#include <stdlib.h>
#define INFORMA printf

char *program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
    Usage: %s <numero>\n\n\
    Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}

int entrada(int numero, int argc, char *argv[]){

    if(argc < 2){
        print_usage(1);
    }
    numero = atoi(argv[1]);
   
}
int mayor(int numero, int mul, int sum, int res_mul, int res_sum){

    if(numero > 10){
  
        for(int i = 0; i<=numero; i++)
        res_mul *= i;
        /*SALIDA*/ INFORMA("%i\n", res_mul);
    }else{
    
        for(int i=0; i<=numero; i++)
        res_sum += i;
        /*SALIDA*/ INFORMA("%i\n", res_sum);
    }
}

int main(int argc, char *argv[]){
    int numero;
    int mul;
    int sum;
    int res_mul = 1;
    int res_sum = 0;
    program_name = argv[0];

    /*ENTRADA*/ numero = entrada(numero, argc, argv);
    
    /*CÁLCULOS*/ mayor(numero, mul, sum, res_mul, res_sum);

    return EXIT_SUCCESS;
}
