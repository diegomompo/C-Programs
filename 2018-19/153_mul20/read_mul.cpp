#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define OUT "multiply.dat"
#define INFORM printf


int main(int argc, char *argv[]){

    int multi[20];
    FILE *pf;

    if(!(pf = fopen(OUT, "rb"))){
        fprintf(stderr, "Can't file open %s", OUT);
        return EXIT_FAILURE;
    }

    for(int i=0; i<20; i++){
      fread(&multi, sizeof(int), 20, pf);
      printf("%i, ", multi[i]);
    }

    fclose(pf);

    printf("\n");


    return EXIT_SUCCESS;
}
