#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define fINFORMA fprintf
#define OUT "suma_factorial.txt"

char * program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
     Usage: %s <numero>\n\n\
     Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}
int entrada(int numero, int argc, char *argv[]){

    if(argc < 2)
        print_usage (1);

    numero = atoi(argv[1]);

    return numero;
}
int suma_factorial(int numero){

    if(numero == 0)
        return -1;

    return numero + suma_factorial(numero-1);
}

int main(int argc, char *argv[]){

    FILE *sum_fac;
    int numero;

    program_name = argv[0];


    /*ENTRADA*/ numero = entrada(numero, argc, argv);
    
    /*SALIDA*/

    if(!(sum_fac = fopen(OUT, "w")))
        return EXIT_FAILURE;
    fINFORMA(sum_fac, "La suma de los %i primeros números es %i\n", numero, suma_factorial(numero));

    fclose(sum_fac);


    return EXIT_SUCCESS;
}
