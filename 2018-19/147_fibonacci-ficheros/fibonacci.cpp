#include <stdio.h>
#include <stdlib.h>
#define fINFORMA fprintf
#define OUT "fib_pascal.txt"

char * program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
     Usage: %s <numero>\n\n\
     Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}
int entrada(int numero, int argc, char *argv[]){

    if(argc < 2)
        print_usage (1);

    numero = atoi(argv[1]);

    return numero;
}

int fibonacci(int numero, int q, int y, int z, FILE *fb){
    
    q=0;
    y=1;

    for(int i=1; i<=numero; i++){
        
        z=q+y;
        fINFORMA(fb, "%i, ", z);
        q=y;
        y=z;
        
    }
    fINFORMA(fb, "\n");
    
}
int pascal(int numero, int fibo[], int i, int j, int x, FILE *fb){

    x = 0;

    for (i=1; i<=numero; i++){

        for(j=x; j>=0; j--){

            if(j==x || j==0){
                fibo[j] = 1;
            }
            else{
                fibo[j] += fibo[j-1];
            }
        }
      x++;

        fINFORMA(fb, "\n");

        for(j=1; j<=numero-i; j++)
            fINFORMA(fb, "    ");

        for(j=0; j<x; j++){

            fINFORMA(fb, "%3d    ", fibo[j]);
        }

    }
    fINFORMA(fb, "\n");

}

int main(int argc, char *argv[]){

    int numero, fibo[numero], i, j, q, x, y, z;
    FILE *fb;
    program_name = argv[0];


    /*ENTRADA*/ numero = entrada(numero, argc, argv);

    /*SALIDA*/

    if(!(fb = fopen(OUT, "w")))
        return EXIT_FAILURE;

        fibonacci(numero, q, y, z, fb);

        pascal(numero, fibo, i, j, x, fb);

    fclose(fb);

    return EXIT_SUCCESS;
}
