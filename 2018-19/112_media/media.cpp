#include <stdio.h>
#include <stdlib.h>

#define N 3
int main(){

    double num[N];
    double suma = 0;
    double media;

    // ENTRADA
    
    for(int i=0; i<N; i++){
        scanf("%lf", &num[i]);
        suma = suma + num[i];
    }

    // CÁLCULOS

    media = suma / N;
    
    // SALIDA
    
    printf("%.2lf\n", media);

    return EXIT_SUCCESS;
}
