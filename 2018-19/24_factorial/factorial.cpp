#include <stdio.h>
#include <stdlib.h>

#define INFORMA printf

char * program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
     Usage: %s <numero>\n\n\
     Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}
int entrada(int numero, int argc, char *argv[]){

    if(argc < 2)
        print_usage (1);
    numero = atoi(argv[1]);

    return numero;

}

int factor(int numero){

    if(numero == 0)
        return -1;

    return numero*factor(numero-1);
}

int main(int argc, char *argv[]){
    int numero;
    int res;
    program_name = argv[0];

    /*ENTRADA*/ numero = entrada(numero, argc, argv);
    
    /*CÁLCULOS*/
    
    /*SALIDA*/ INFORMA("La multiplicación de %i primeros números es %i\n", numero, factor(numero));

    return EXIT_SUCCESS;
}
