#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 2
#define CUARTO 250000
#define MEDIO 400000
#define UNO 1000000
#define DOS 2000000

int Parte2_ESP(){	
	
    for(int i=0; i<VECES; i++){
	fputc('\a', stderr);
	usleep(UNO);
	fputc('\a', stderr);
	usleep(UNO);
	fputc('\a', stderr);
	usleep(CUARTO);
	fputc('\a', stderr);
	usleep(UNO);
	fputc('\a', stderr);
	usleep(UNO);
	fputc('\a', stderr);
	usleep(CUARTO);
	fputc('\a', stderr);
	usleep(UNO);
	fputc('\a', stderr);
	usleep(UNO);
	fputc('\a', stderr);
	usleep(CUARTO);
	fputc('\a', stderr);
	usleep(MEDIO);
	fputc('\a', stderr);
	usleep(MEDIO);
	fputc('\a', stderr);
	usleep(MEDIO);
	fputc('\a', stderr);
	usleep(MEDIO);
	fputc('\a', stderr);
	usleep(UNO);
	fputc('\a', stderr);
	usleep(UNO);
	fputc('\a', stderr);
	usleep(UNO);
	fputc('\a', stderr);
	usleep(CUARTO);
	fputc('\a', stderr);
	usleep(MEDIO);
	fputc('\a', stderr);
	usleep(MEDIO);
	fputc('\a', stderr);
	usleep(UNO);
	fputc('\a', stderr);
	usleep(UNO);
	fputc('\a', stderr);
	usleep(DOS);

   }

    return EXIT_SUCCESS;
}
