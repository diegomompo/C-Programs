#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "define_color.h"
#include "math.h"


#define N     100
#define DELAY 100000


int cargando(){
   
  for (int veces=0; veces<N; veces++){
    for (int columna=0; columna<veces; columna++)
      fprintf(stderr, BLUE"=");
    fprintf(stderr, BLUE "Cargando 2 jugadores %i%%\r",veces);
    usleep(DELAY);
  }
  printf(RESET_COLOR "\n");
}

// -----------------------------------------------------------------------------
int imprimiendo(){

  for (int veces=0; veces<N; veces++){
    for (int columna=0; columna<veces; columna++)
      fprintf(stderr, BLUE "=");
    fprintf(stderr, BLUE "Imprimiendo puntuaciones %i%%\r",veces);
    usleep(DELAY);
  }
  printf(RESET_COLOR "\n");
}
