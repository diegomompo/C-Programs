#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "define_color.h"
#include "math.h"

enum { // ENUMERO LAS OPCIONES DEL MENÚ
 
 triangulo,
 rectangulo,
 cuadrado,
 circulo,
 FIGURAS

};
// -----------------------------------------------------------------------------
void menu(){ // FUNCIÓN MENÚ

   printf("\n"
	 BLUE   "\tLista de Figuras\n" RESET_COLOR
         YELLOW "\t================\n" RESET_COLOR
	 "\n"
	 GREEN  "\t1. " RESET_COLOR "Triángulo.\n"
	 GREEN  "\t2. " RESET_COLOR "Rectángulo.\n"
	 GREEN  "\t3. " RESET_COLOR "Cuadrado.\n"
	 GREEN  "\t4. " RESET_COLOR "Círculo.\n"

	 "\tOpción: ",
	 FIGURAS
	 );

}
