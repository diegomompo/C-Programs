#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){

    double diam;
    double longitud;

    // ENTRADA
    
    scanf("%lf", &diam);
    
    // CÁLCULOS

    longitud = M_PI * diam;
    
    // SALIDA
    
    printf("%.2lf\n", longitud);

    return EXIT_SUCCESS;
}
