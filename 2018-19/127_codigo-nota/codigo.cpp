#include <stdio.h>
#include <stdlib.h>
#include "barra.h"

#define N 5

int main(){

    char nota;
    char evaluacion[N] = {'A', 'B', 'C', 'D', 'F'};

    // ENTRADA

    printf("Nota: ");
    scanf("%c", &nota);
    
    // CÁLCULOS

    cargando();

    if(nota == evaluacion[0]){

        evaluando();
        printf("Excelente\n");
    }
    else if(nota == evaluacion[1]){       
            
        evaluando();
        printf("Notable\n");
    }
    else if(nota == evaluacion[2]){       
            
        evaluando();
        printf("Aprobado\n");
    }
    else if(nota == evaluacion[3] || nota == evaluacion[4]){       
            
        evaluando();
        printf("Repobrado\n");
    }
    else{
    
    printf("Lo siento, la nota introducida no es correcta\n");

    }

    return EXIT_SUCCESS;
}
