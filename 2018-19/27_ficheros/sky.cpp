#include <stdio.h>
#include <stdlib.h>

#define NOMBRE "cancion.txt"

const char * song = "\n\
                Don't think sorry's easily saiz\n\
                Don't try turning tables instead\n\
                You've taken lots of Chances before\n\
                But I'm not gonna give anymore\n\
                Don't ask me\n\
                That's how it goes\n\
                'Cause part of me knows what you're thinkin'\n\
                Don't say words you're gonna regret\n\
                Don't let the fire rush to your head\n\
                I've heard the accusation before\n\
                And I ain't gonna take any more\n\
                Believe me\n\
                he sun in your Eyes\n\
                Made some of the lies worth believing\n\
                I am the eye in the sky\n\
                Looking at you\n\
                I can read your mind\n\
                ";

void print_usage(){
    printf("Esto se usa asi\n");
}

void informo (const char * mssg){
    print_usage();
    fprintf(stderr, "%s\n", mssg);
    exit(1);
}
int main(int argc, char *argv[]){

    FILE *fichero;

    if( !(fichero = fopen( NOMBRE, "w")))
        informo ("No se ha podio abrir el fichero.");
    fprintf (fichero, "%s", song);
    getchar ();
    fclose(fichero);

    return EXIT_SUCCESS;
}

