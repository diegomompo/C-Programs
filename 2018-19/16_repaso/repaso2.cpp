#include <stdio.h>
#include <stdlib.h>

#define N 1000

// VERSION PROFE

int main(){

    int n_alumnos = 0;
    double media;
    double entrada;
    double nota[N];

    /* Entrada de datos */

    do{
        printf("Niño %i: ", n_alumnos);
        scanf("%lf", &entrada);
        if(entrada >=0)
            nota[n_alumnos++] = entrada;
    }while (entrada>=0);

    printf("\n");

    /* Cálculos */

    for( int i = 0; i<n_alumnos; i++){
        media += nota[i];
    }

    media /= n_alumnos;
    
    /* Salida de datos */

    printf("%.2lf\n", media);

    return EXIT_SUCCESS;
}

// --------------------------------------------------------------------------------------------------

// MI VERSION

/*  int nota;
    int media;
    int suma = 0;
    int almacen[N];

     Entrada de datos 

    for(int i = 0; i < N; i++){
    
        printf("Niño %i: ", i);
        scanf("%d", &nota);
        almacen[i] = nota;
        suma = suma + almacen[i];
    }

    printf("\n");

     Cálculos 

    media = suma / N;
    
     Salida de datos 

    printf("%I\n", media);


    return EXIT_SUCCESS;
} */
