#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "define_color.h"
#include <strings.h>

#define N 100

// JUEGO DEL AHORCADO EN C
// BASADO EN PROGRAMAS DE MI PROFESOR Y EN UN PROGRAMA SACADO DE INTERNET
// http://programavideojuegos.blogspot.com/2013/05/juego-el-ahorcado-en-c.html

int main() {
    char word[N],rep[N],temporal[N];
    char letter;
    int len, length,i,j,start,right=0,temp=0,opor=7;
    int repeated=0;
    bool gano = false;
   
    printf(RED "\tHangman game\n" RESET_COLOR);

    // INICIO INTRODUCE LA PALABRA
    
    printf("Enter the word to guess: ");
    scanf("%s", word);

    // FIN INTRODUCE LA PALABRA
   
    system("clear");
   
    length = 0;
    start = 0;
    j = 0;
   
    len = strlen(word);

    rep[0] = ' ';
    rep[1] = '\0';
   
   
    do {
        system ("clear");
        temp=0;
   
        // INICIO GUARDAR BARRA BAJA

        if(start == 0) {
         for(i=0;i<len-1;i++) {
          if(word[i] == ' ') {
            temporal[i] = ' ';
             length++;
          }
          else {
             temporal[i] = '_';       
             length++;
          }
         }
        }

        // FIN GUARDAR BARRA BAJA

        start = 1;

        temporal[length] = '\0';

        // INICIO COMPROBAR SI EXISTE LA LETRA

        for(i=0;i<strlen(rep);i++) {
            if(rep[i] == letter) {
                repeated = 1;
                break;
            }
            else {
                repeated = 0;
            }
        }

        // FIN COMPROBAR SI EXISTE LA LETRA
        
        // INICIO COMPROBAR SI LA LETRA ES ACERTADA O NO 

        if(repeated == 0) {
            for(i=0;i<len-1;i++) {
                if(word[i] == letter) {
                    temporal[i] = letter;
                    right++;
                    temp=1;
                }
            }
        }

        if(repeated == 0) {
            if(temp == 0) {
                opor -= 1;
            }
        }
        else {
            printf("this character has already been introduced");
            printf("\n\n");
        }

        // FIN COMPROBAR SI LA LETRA ES ACERTADA O NO

        printf("\n");

        // INICIO IMPRIMIR LA PALABRA

        for(i=0;i<strlen(temporal);i++) {
            printf(" %c ",temporal[i]);
        }

        // FIN IMPRIMIR LA PALABRA

        printf("\n");

        // INICIO COMPROBAR SI GANO

        if(strcmp(word,temporal) == false) {
            gano = true;
            break;
        }

        // FIN COMPROBAR SI GANO

        // INICIO IMPRIMIR LETRAS ACERTADAS Y OPORTUNIDADES RESTANTES

        printf("\n");
        printf("Letras Acertadas: %d",right);
        printf("\n");
        printf("Oportunidades Restantes: %d",opor);
        printf("\n");

        rep[j] = letter;
        j++;

        if (opor==0)
        {
            break;
        }

        // FIN IMPRIMIR LETRAS ACERTADAS Y OPORTUNIDADES RESTANTES
        
        // INICIO INTRODUCIR LETRA

        printf("Introduzca una letra:");
        scanf("\n%c",&letter);

    }while(opor != 0 && gano == false);

    // INICIO IMPRIMIR SI SE HA GANADO O NO

    if(gano){
        printf("\n\n");
        printf("Congratulations. You are win.");
    }
    else {
        printf("\n\n");
        printf("Hangman jejejejeje. The word was %s .", word);
    }

    // FIN IMPRIMIR SI SE HA GANADO O NO

    printf("\n\n"); 
    return 0;

}  
