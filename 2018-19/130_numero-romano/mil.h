#include <stdio.h>
#include <stdlib.h>

enum {
    m_mil,
    mil,
    dosmil,
    tresmil,
    cuatromil,
    cincomil,
    seismil,
    sietemil,
    ochomil,
    nuevemil
};

void millar2(int millar){

    switch(millar){
   
        case m_mil:
            printf("");
            break; 
        case mil:
            printf("M");
            break;
        case dosmil:
            printf("MM");
            break;
        case tresmil:
            printf("MMMM");
            break;
        case cuatromil:
            printf("IV");
            break;
        case cincomil:
            printf("V");
            break;
        case seismil:
            printf("VI");
            break;
        case sietemil:
            printf("VII");
            break;
        case ochomil:
            printf("VIII");
            break;
        case nuevemil:
            printf("IX");
            break;
        break;
    }
}    
