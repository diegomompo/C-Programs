#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIM 3

int main(){

    double vec[DIM];
    double modulo;

    scanf("%lf,%lf,%lf", &vec[0], &vec[1], &vec[2]);

    modulo = sqrt((pow(vec[0],2) + pow(vec[1],2) + pow(vec[2],2)));

    printf("%.2lf\n", modulo);

    return EXIT_SUCCESS;
}
