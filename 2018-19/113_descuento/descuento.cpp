#include <stdio.h>
#include <stdlib.h>

#define D 15
#define P 100

int main(){

    double total, descuento, tot_descuento;
    // ENTRADA

    scanf("%lf", &total);
    
    // CÁLCULOS

    descuento = (total * D)/P;

    tot_descuento = total - descuento;
    
    // SALIDA

    printf("%.2lf\n", tot_descuento);

    return EXIT_SUCCESS;
}
