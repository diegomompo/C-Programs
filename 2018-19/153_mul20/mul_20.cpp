#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define OUT "multiply.dat"
#define N 20
#define fINFORM fprintf

char * program_name;

void print_usage(FILE *outstream, int exit_code){

    fINFORM(outstream,
    "This program multiply a number introduce for the user and the twenty first numbers. - \n\n"
    "Usage:"
    "-h <help> \t h: Help the program\n\n"
    "-i <number> \t i: Introduce a number\n"
    "-m <number> \t m: Calculated the number multiply" 
    "Example: %s <numero> -m\n\n"
    "Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}
int entry(int number, int argc, char *argv[]){

    if(argc < 1)
        return EXIT_FAILURE;

    number = atoi(argv[2]);

    return number;
}

int mul(int number, int multi, FILE *pf){

    for(int i=1; i<=N; i++){
        multi = number * i;
        fwrite(&multi, sizeof(int), 1, pf);
        }
}

int main(int argc, char *argv[]){

    int number;
    int multi;
    int c;
    FILE *pf;

    program_name = argv[0];

    if(!(pf = fopen(OUT, "wb"))){
      fINFORM(stderr, "Can't file open %s", OUT);
      return EXIT_FAILURE;
    }

    while((c = getopt(argc, argv, "h:i:m")) != -1){

      switch(c){  
          case 'h':
              print_usage(stdout, 0);
              break;
          case 'i':
            /*ENTRY*/ number = entry(number, argc, argv);
            break;
          case 'm':
            /*CALCULATED AND EXIT*/ mul(number, multi, pf);
            break;
          case '?':
            if(optopt != 'i' && optopt != 'm')
                fINFORM(stderr, "Invalid Option\n");
            else
                fINFORM(stderr, "Option argument missing\n");

            print_usage(stderr, 1);
            break;
          default:
            abort();
            break;
      }
    }

    fclose(pf);

    return EXIT_SUCCESS;
}
