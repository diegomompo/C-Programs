#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "define_color.h"
#include "math.h"

#define MAX 7.0
#define MIN 0.5

float contador_j1 = 0;
float contador_j2 = 0;

void jugador1(){ // JUGADOR 1
    char respuesta_j1;
    float numero_j1;

    do{

    printf("¿Desea pedir carta Jugador 1?: ");

    scanf("%c", &respuesta_j1 );

      if(respuesta_j1 == 's'){

      srand48(time(NULL)); 

      numero_j1 = drand48() * (MAX-MIN) + MIN;
    
      if(numero_j1 < 1 && numero_j1 > 0.5){
              numero_j1 = 0.5;
      }
      else if(numero_j1 < 1.5 && numero_j1 > 1){
              numero_j1 = 1;
      }
      else if(numero_j1 < 2.5 && numero_j1 > 1.4){
              numero_j1 = 2;
      }
      else if(numero_j1 < 3.5 && numero_j1 > 2.4){
             numero_j1 = 3;
      }
      else if(numero_j1 < 4.5 && numero_j1 > 3.4){
              numero_j1 = 4;
      }
      else if(numero_j1 < 5.5 && numero_j1 > 4.4){
              numero_j1 = 5;
      }
      else if(numero_j1 < 6.5 && numero_j1 > 5.4){
              numero_j1 = 6;
      }
      else if(numero_j1 < 7 && numero_j1 > 6.4){
              numero_j1 = 7;
      }

      contador_j1 = contador_j1 + numero_j1;

      printf("Tu puntuación es %.1f\n", contador_j1);

    }

    if(respuesta_j1 == 'n')
    break;
      
   }while(contador_j1 <= 7.5);

    system("clear");
      
}

void jugador2(){ // JUGADOR 1
    char respuesta_j2;
    float numero_j2;

    do{

    printf("¿Desea pedir carta Jugador 2?: ");

    scanf("%c", &respuesta_j2 );

      if(respuesta_j2 == 's'){

      srand48(time(NULL)); 

      numero_j2 = drand48() * (MAX-MIN) + MIN;
    
      if(numero_j2 < 1 && numero_j2 > 0.5){
              numero_j2 = 0.5;
      }
      else if(numero_j2 < 1.5 && numero_j2 > 1){
              numero_j2 = 1;
      }
      else if(numero_j2 < 2.5 && numero_j2 > 1.4){
              numero_j2 = 2;
      }
      else if(numero_j2 < 3.5 && numero_j2 > 2.4){
             numero_j2 = 3;
      }
      else if(numero_j2 < 4.5 && numero_j2 > 3.4){
              numero_j2 = 4;
      }
      else if(numero_j2 < 5.5 && numero_j2 > 4.4){
              numero_j2 = 5;
      }
      else if(numero_j2 < 6.5 && numero_j2 > 5.4){
              numero_j2 = 6;
      }
      else if(numero_j2 < 7 && numero_j2 > 6.4){
              numero_j2 = 7;
      }

      contador_j2 = contador_j2 + numero_j2;

      printf("Tu puntuación es %.1f\n", contador_j2);

    }

    if(respuesta_j2 == 'n')
    break;
      
   }while(contador_j2 <= 7.5);

    system("clear");
      
}


void puntuacion(){

printf("\n\n"
      YELLOW "\t==========" "========================================" "\n"
      YELLOW "\t          "  GREEN " Puntuacion JUGADOR 1 = %.1f     " "\n"
      YELLOW "\t          "  GREEN " Puntuacion JUGADOR 2 = %.1f     " "\n" 
      YELLOW "\t==========" "========================================"  RESET_COLOR "\n\n", contador_j1, contador_j2);
}
