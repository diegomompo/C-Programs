#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIM 2

int main(){

    double vec[DIM];
    double modulo;

    scanf("%lf,%lf", &vec[0], &vec[1]);

    modulo = sqrt((pow(vec[0],2) + pow(vec[1],2)));

    printf("%.2lf\n", modulo);

    return EXIT_SUCCESS;
}
