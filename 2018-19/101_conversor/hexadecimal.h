#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void Hexadecimal_Octal(){
   
    int hex_oct;

    printf("Ingrese un número para convertirlo en Octal: ");
    scanf("%X", &hex_oct);

    printf("\n");

    printf("EL número en Octal es 0x%o\n", hex_oct);
}

void Hexadecimal_Binario(){
    
    int hex_bin;

    printf("Ingrese un número para convertirlo en Binario: ");
    scanf("%X", &hex_bin);

    int binario = 0;
    int posicion = 1;

    while(hex_bin > 0){
	 binario = binario + (hex_bin%2)*posicion;
	 hex_bin /=2;

	 posicion *=10;
    }	 

    printf("\n");

    printf("El número convertido en Binario es: %d\n", binario);

 }

void Hexadecimal_Decimal(){
	
    int hex_dec;


    printf("Ingrese un número para convertirlo en Decimal: ");
    scanf("%X", &hex_dec);


    printf("El número  convertido en Decimal es %d\n", hex_dec);

    

}

