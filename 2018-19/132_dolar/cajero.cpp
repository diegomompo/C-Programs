#include <stdio.h>
#include <stdlib.h>
#include "menu.h"
#include "barra.h"

enum{

  consulta,
  ingreso,
  retiro,
  salir 
};

int entrada(int opcion){

    menu();
    scanf("%i", &opcion);

    return opcion;
}

void consultar(double salario){

    consultando();
    printf("El salario en la cuenta es %.2lf euros\n", salario);

}

double ingresar(double sal_ingreso, double salario){

    printf("Introduce el salario a ingresar: ");
    scanf("%lf", &sal_ingreso);

    ingresando();

    salario = salario + sal_ingreso;

    printf("Ha ingresado %.2lf euros. El salario en la cuenta es %.2lf\n", sal_ingreso, salario);

}

double retirar(double sal_retiro, double salario){

    printf("Introduce el salario a retirar: ");
    scanf("%lf", &sal_retiro);

    retirando();

    salario = salario - sal_retiro;

    printf("Ha ingresado %.2lf euros. El salario en la cuenta es %.2lf\n", sal_retiro, salario);
}

void calculo(int opcion, double salario, double sal_ingreso, double sal_retiro){

    switch(opcion){
      case consulta:
         consultar(salario);
      break;
      case ingreso:
         salario = ingresar(sal_ingreso, salario);
      break;
      case retiro:
         salario = retirar(sal_retiro, salario);
      break;
      case salir:
        printf("Para salir, presiona CTRL+C\n");
      break;
      default:
        printf("Opción incorrecta.\n");
    }
}

int main(){

    int opcion;
    double salario = 1000;
    double sal_ingreso;
    double sal_retiro;

    // ENTRADA

    system("clear");

    inicio:

    opcion = entrada(opcion);

    cargando();
    
    // CÁLCULOS
    
    calculo(opcion, salario, sal_ingreso, sal_retiro);

    goto inicio;

    return EXIT_SUCCESS;
}
