#include <stdio.h>
#include <stdlib.h>

int main(){

    int numerador;
    int denominador;
    double decimal;

    scanf("%d/%d", &numerador, &denominador);

    do{

            if(numerador%2==0 && numerador>=2){
        
              numerador = numerador/2;
              denominador = denominador/2;
            }
            else if(numerador%5==0 && numerador>=5){
        
              numerador = numerador/5;
              denominador = denominador/5;
            }
            else{
        
              break;
            }
    
    }while(((numerador%2 == 0 && numerador >=2) || (numerador%5 == 0 && numerador >=5)) && ((numerador%2 == 0 && numerador >=2) || (numerador%5 == 0 && numerador >=5)));

    decimal = (double) numerador / (double) denominador;

    printf("%d/%d = %.4lf\n", numerador, denominador, decimal);

    return EXIT_SUCCESS;
}
