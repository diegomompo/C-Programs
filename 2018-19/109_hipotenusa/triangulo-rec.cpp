#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define CAT 2
int main(){

    double cateto[CAT];
    double hipotenusa;

    // ENTRADA
    
    for(int i=0; i<CAT; i++)
        scanf("%lf", &cateto[i]);
    
    // CÁLCULOS
    
    hipotenusa = sqrt(pow(cateto[0], 2) + pow(cateto[1], 2));
    
    // SALIDA
    
    printf("%.2lf\n", hipotenusa);

    return EXIT_SUCCESS;
}
