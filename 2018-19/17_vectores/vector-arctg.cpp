#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define X 0
#define Y 1
#define DIM 2

double modulo(double vec[DIM]){

    return sqrt(pow(vec[X],2) + pow(vec[Y],2));
}

// ----------------------------------------------------------------------------------------

int main(){

    double vec[2][DIM],
           modulo[2],
           producto = 0,
           angulo;

    // ENTRADA

    for(int i=0; i<2; i++) 
    scanf("%lf,%lf", &vec[i][X], &vec[i][Y]);

    // CÁLCULO

    for(int c=0; c<DIM; c++)
    producto += (vec[0][c] * vec[1][c]);
    
    angulo = acos(producto / modulo(vec[0]) / modulo(vec[1]);
    angulo *= 180 / M_PI;

    // SALIDA
    printf("%.2lf grados\n", angulo);


    return EXIT_SUCCESS;
}
