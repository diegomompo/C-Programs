#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <math.h>

#include "naca_23015.h"

#define MAX 0x100
#define X 0
#define Y 1

void borra(void *donde, int tam, int cuanto1, int cuanto2){
    bzero (donde, tam * cuanto1 * cuanto2);

}
double modulo (double p[2]){
   return sqrt (p[0] * p[0] + p[1] * p[1]);
}
void imprime (
        const double puntos[][2],
	double desp[][2],
	double s[],
	double tan[][2],
	int n_puntos)
{
   for (int p=0; p<n_puntos; p++){
      printf("(%.4lf, %.4lf)\t", puntos[p][X], puntos[p][Y]);
      printf("(%.4lf, %.4lf)\t", desp[p][X], desp[p][Y]);
      printf("- %.4lf -\t", s[p]);
      printf("(%.4lf, %4.lf)\t", tan[p][X], tan[p][Y]);
   }
   printf("\n");
}

int main(){
    double s[MAX],
           d[MAX][2],
	   t[MAX][2];
    int n_puntos = sizeof(perfil) /  sizeof(double) / 2;
    
    // CÁLCULOS

    borra ((void *) s, sizeof(double), MAX, 1);
    borra ((void *) d, sizeof(double), MAX, 2);
    borra ((void *) t, sizeof(double), MAX, 2);
    
    for (int p=0; p<n_puntos-1; p++){
	/* Vector desplazamiento entre puntos */
	d[p][X] = perfil[p+1][X] - perfil[p][X];
	d[p][Y] = perfil[p+1][Y] - perfil[p][Y];
	/* Espacio entre puntos */
	s[p] = modulo (d[p]);
	/* Vector tangente en cada punto */
	t[p][X] = d[p][X] / s[p];
	t[p][Y] = d[p][Y] / s[p];
    }
    
    // SALIDA

    imprime (perfil, d, s, t, n_puntos);

    return EXIT_SUCCESS;
}
