#include <stdio.h>
#include <stdlib.h>
#include "define_color.h"
#include "math.h"
#include "menu.h"
#include "barra.h"
#include "jugador.h"

int main(){

    int opcion;
    int ronda;

    otra_ronda:

    system("clear");
    printf(YELLOW "\t JUEGO D" RED "E  CARTA" BLUE "S SIETE" GREEN " Y MEDIO\n");

    menu();

    scanf("%i", &opcion);

    switch(opcion){

        case dos_jugadores:
        system("clear");
        cargando();
        jugador1();
        jugador2();
        imprimiendo();
        puntuacion();
        break;
    
    }

    printf("¿Desea jugar otra ronda(S/N)?: ");
    scanf("%i", ronda);

    if(ronda == 's'){
    
      goto otra_ronda;
    }
    else{
      
      printf("GRACIAS. HASTA LUEGO\n");  
    }

    return EXIT_SUCCESS;
}
