#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include "funciones.h"
#define N 10

struct TPila{
  double sav_num[N];
  int cima;
  int cabeza;
};

void push(int numero, struct TPila *p){
  
      p->sav_num[p->cima] = numero;
      p->cima++;
}

double intro(double  numero, struct TPila *pila){
 
      for(int i = 0; i < N; i++){
           printf("Número: ");
           scanf("%lf", &numero);
           __fpurge(stdin);
           push(numero, pila);
       }
          printf("\n");
}
