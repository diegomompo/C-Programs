#include <stdio.h>
#include <stdlib.h>

#define X 0
#define Y 1
#define DIM 2

int main(){

    int vector[2][DIM], producto;

    for(int i=0; i<DIM; i++){
     /* ENTRADA */  scanf("%d,%d", &vector[i][X], &vector[i][Y]);

     /* CÁLCULO */  producto = ((vector[0][X] * vector[1][Y]) - (vector[1][X] * vector[0][Y]));
    }

    /* SALIDA */ printf("%d\n", producto);

    return EXIT_SUCCESS;
}
