#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INFORM printf
#define fINFORM fprintf
#define INTRODUCE scanf
#define OUT "nombre.txt"
#define MAX 0x100
#define N 10

char name[N][MAX];

void entry(char name[N][MAX]){

    for(int i = 0; i<N; i++){
        INFORM("Name %i: ", i);
        fgets(name[i], MAX, stdin);
    }
    
}
void volcar(char name[N][MAX], FILE *nm){
    for(int i=0; i<N; i++)
      fINFORM(nm, "%s", name[i]);
}

int main(int argc, char *argv[]){

    FILE *nm;

    /*ENTRADA*/ entry(name);
    
    /*SALIDA*/

    nm = NULL;

    if(!(nm = fopen(OUT, "w")))
        return EXIT_FAILURE;

    volcar(name, nm);

    fclose(nm);

    return EXIT_SUCCESS;
}
