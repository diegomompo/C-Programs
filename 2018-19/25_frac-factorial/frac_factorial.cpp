#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define INFORMA printf
#define E 0.00000000001

char * program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
     Usage: %s <Numero>\n\n\
     Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}

double entrada_base(int base, int argc, char *argv[]){

    if(argc < 2)
        print_usage (1);
    base = atoi (argv[1]);

    return base;
}
double factorial(int base, int prof){

    if(prof == 1)
        return base;
    
    return base + 1./factorial(base,prof - 1);
}

int main(int argc, char *argv[]){

    int base, i;
    double res0 = 0, res1 = -100;

    program_name = argv[0];

    /*ENTRADA*/ 
    base = entrada_base(base, argc, argv);
        
    /*CÁLCULOS*/

    for(i=1; fabs(res1 - res0) > E && i < 20; i++){
        res0 = res1;
        res1 = factorial(base, i);
   }
    /*SALIDA*/
    
    INFORMA("Fracción continua (%i, %i) = %.10lf\n", base, i, res1);

    return EXIT_SUCCESS;
}
