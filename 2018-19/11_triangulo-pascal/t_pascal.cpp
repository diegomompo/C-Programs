#include <stdio.h>
#include <stdlib.h>

#define N 13
int main(){

    int pascal[N];
    int i, j;
    int X = 0;

    for (i=1; i<=N; i++){
    
        for(j=X; j>=0; j--){
        
            if(j==X || j==0){
                pascal[j] = 1;
            }
            else{
            
                pascal[j] = pascal[j] + pascal[j-1];
            }
        }
      X++;

        printf("\n");

        for(j=1; j<=N-i; j++)
            printf("    ");

        for(j=0; j<X; j++){
        
            printf("%3d    ", pascal[j]);  
        }

    }
    printf("\n");

    return EXIT_SUCCESS;
}
