#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define fINFORM fprintf
#define FILENAME "nombre.txt"
#define MAX 0x100
#define N 10


int main(int argc, char *argv[]){

    char nombre[N][MAX];
    FILE *mf;

    /*SALIDA*/

  
    if(!(mf = fopen(FILENAME, "r"))){
        fINFORM(stderr, "No se puede abrir el fichero\n");
        return EXIT_FAILURE;
    }
    for(int i=0; i<N; i++)
        fgets(nombre[i], MAX, mf);

    for(int i=0; i<N; i++)
        printf("Fila %i: %s\n", i, nombre[i]);

    fclose(mf);

    return EXIT_SUCCESS;
}
