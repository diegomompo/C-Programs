#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define INFORM printf
#define fINFORM fprintf
#define OUT "random.txt"
#define MAX 0x100
#define N 10

char res[N];

void push(char name){
  static int i=0;
  res[i++] = name;
}

char entry(char name){

    for(int i = 0; i<N; i++){
        INFORM("Name %i: ", i);
        scanf("%s", &name);
        push(name);
    }
    
}
void volcar(int pos, char res[MAX], FILE *nm){
    
    for(int i=0; i<N; i++){
      fINFORM(nm, "%s", res[i]);
    }
}

int main(int argc, char *argv[]){

    FILE *nm;
    char name;
    int pos;

    /*ENTRADA*/ entry(name);
    
    /*SALIDA*/

    srand(time(NULL));

    if(!(nm = fopen(OUT, "w")))
        return EXIT_FAILURE;

    volcar(pos, res, nm);

    fclose(nm);

    return EXIT_SUCCESS;
}
