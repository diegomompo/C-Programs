#include <stdio.h>
#include <stdlib.h>

int  hipotenusa(){

int cateto1;
int cateto2;
int hipotenusa;

printf("Introduce el primer cateto: ");
scanf("%i", &cateto1);
printf("Introduce el segundo cateto: ");
scanf("%i", &cateto2);

hipotenusa = sqrt(cateto1 * cateto1 + cateto2 * cateto2);

printf("La hipotenusa del triángulo es %i \n", hipotenusa);

}
