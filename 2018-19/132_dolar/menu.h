#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "define_color.h"
#include "math.h"

// -----------------------------------------------------------------------------
void menu(){ // FUNCIÓN MENÚ

   printf("\n"
	 BLUE   "\tCAJERO AUTOMÁTICO\n" RESET_COLOR
         YELLOW "\t================\n" RESET_COLOR
	 "\n"
	 GREEN  "\t1. " RESET_COLOR "Consultar dinero.\n"
	 GREEN  "\t2. " RESET_COLOR "Ingresar dinero.\n"
	 GREEN  "\t3. " RESET_COLOR "Retirar dinero.\n"
	 GREEN  "\t4. " RESET_COLOR "Salir.\n"

	 "\tOpción: "
	 );

}
