#include <stdio.h>
#include <stdlib.h>
#define INFORMA printf
#define D 2

char* program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
     Usage: %s <numero>\n\n\
     Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}
int entrada_ini(int n_ini, int argc, char *argv[]){

    if(argc < 2){
        print_usage (1);
    }
    n_ini = atoi(argv[1]);
}
int entrada_fin(int n_fin, int argc, char *argv[]){

    if(argc < 2){
        print_usage (1);
    }
    n_fin = atoi(argv[2]);
}
int suma_par(int n_ini, int n_fin, int suma){
    suma = 0;
    for(int i=n_ini; i<=n_fin; i++){
      if(i%D == 0){
         suma += i;   
      }
    }
    return suma;
}
   


int main(int argc, char *argv[]){
    int n_ini;
    int n_fin;
    int suma;
    program_name = argv[0];

    /*ENTRADA*/ 
    n_ini = entrada_ini(n_ini, argc, argv);
    n_fin = entrada_fin(n_fin, argc, argv);
    
    /*CÁLCULOS*/ suma = suma_par(n_ini, n_fin, suma);
    
    /*SALIDA*/ INFORMA("La suma par total es %i\n", suma);

    return EXIT_SUCCESS;
}
