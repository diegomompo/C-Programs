#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "define_color.h"

#define N 100
#define DELAY 50000
#define INC 0.0001

void titulo(){
  
    system("clear");
    system("toilet -fpagga --metal AREA DE PARABOLA");
}


void cargando(void (*in)(void)){
  
    *in;

    struct variable;

    for(int veces = 0; veces<=N; veces++){
        for(int columna = 0; columna < veces; columna++)
            fprintf(stderr, GREEN_CLARO "=");
    fprintf(stderr, "Cargando área de la parábola %i%%\r", veces);
    usleep(DELAY);
    }
    printf(RESET_COLOR);
    
}
double pol(double *coef, int grado, double x){

    int lon = (grado + 1) * sizeof(double);
    double *copia = (double *) malloc (lon);
    double resultado = 0;
    memcpy(*copia, pol, lon);

    for (int i=0; i<grado; i++)
        for(int celda = 0; celda<=i; celda++)
            *(copia + celda) *= x;

    for(int i=0; i<=grado; i++)
        resultado += *(copia + i);

    free (copia);
}

// pol evalua el valor de un polinomio del coeficiente coef en el punto x

double integral (double li, double ls, double (*pf)(double) ){

    double area = 0;
    for(double x=li; x<ls; x+=INC){
        area += INC * (*pf)(x);
    }
    return area;
}

double parabola (double x){
  double coef[] = {1, 0, 0};
  return pol(coef, 2, x);
}

int main(int argc, char *argv[]){
    
    double linf, lsup, grado;

    void (*title)() {&titulo};
    double (*parabol)(double) {&parabola};
    double (*pi)(double, double, double(double)) {&integral};
    void (*barra)(void (void)){ &cargando };

    (*title)();

    (*barra)(&titulo);

    printf("\n");

    printf("f(2) = %lf", (*parabol)(1));
     printf("f(3) = %lf", (*parabol)(3));

    printf("El area es: %lf\n", (*pi)(1, 3, &parabola));

    return EXIT_SUCCESS;
}
