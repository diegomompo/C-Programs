#include <stdio.h>
#include <stdlib.h>

#define P 3
#define PP 0.55
#define PEF 0.30
#define PTF 0.10

double parciales(double nota_parcial[P], double suma_parcial, double promedio_parcial){

    for(int i = 0; i<P; i++){
        
        printf("Examen %i\n", i);
        scanf("%lf", &nota_parcial[i]);
        suma_parcial += nota_parcial[i];
    }
    promedio_parcial = (suma_parcial / P)* PP;

    return promedio_parcial;
}

//-----------------------------------------------------------------
int main(){


    double nota_parcial[P], nota_final, nota_trabajo;
    double suma_parcial, promedio_parcial, porcentaje_examen, porcentaje_trabajo;
    double resultado;

    // ENTRADA
     
    printf("Examen final\n");
    scanf("%lf", &nota_final);

    printf("Trabajo final\n");
    scanf("%lf", &nota_trabajo);
    
    // CÁLCULOS

    porcentaje_examen = nota_final * PEF;
    porcentaje_trabajo = nota_trabajo * PTF;

    resultado = parciales(nota_parcial, suma_parcial, promedio_parcial) + porcentaje_examen + porcentaje_trabajo;
    
    // SALIDA

    printf("Resultado: %.2lf\n", resultado);

    return EXIT_SUCCESS;
}
