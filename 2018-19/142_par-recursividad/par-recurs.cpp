#include <stdio.h>
#include <stdlib.h>
#define INFORMA printf

char * program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
     Usage: %s <numero>\n\n\
     Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}
int entrada(int numero, int argc, char *argv[]){

    if(argc < 2)
        print_usage (1);

    numero = atoi(argv[1]);

    return numero;

}

int par(int numero){

    if(numero == 0)
        return 0;

    return numero + par(numero-2);
}

int main(int argc, char *argv[]){

    int numero;
    int cont;

    program_name = argv[0];

    /*ENTRADA*/ numero = entrada(numero, argc, argv);
    
    /*CÁLCULOS*/

    if(numero%2!=0)
        numero = numero - 1;

    for(int i = 1; i<=numero; i++)
        if(i%2 == 0)
           cont++;    
    
    /*SALIDA*/

    numero = par(numero);

    INFORMA("%i\n", numero);
    INFORMA("%i numeros pares\n", cont);

    return EXIT_SUCCESS;
}
