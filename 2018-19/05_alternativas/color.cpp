#include <stdio.h>
#include <stdlib.h>
#include "../04_operadores/define_color.h"

int main(){

    int color = 0;
    char respuesta;

    printf("¿Ves Rojo (S/N): ");
    scanf(" %c", &respuesta);
    if(respuesta == 's')
    color = ROJO;

    printf("¿Ves Amarillo (S/N): ");
    scanf(" %c", &respuesta);
    if(respuesta == 's')
    color = color + AMARILLO;
  


    printf("¿Ves Azul (S/N): ");
    scanf(" %c", &respuesta);
    if(respuesta == 's')
    color = color + AZUL;


    switch(color){
   
    case 0:     
    printf(BLACK "Ves Negro");
    printf(RESET_COLOR "\n");
    break;

    case 1: 
    printf(BLUE "Ves Azul");
    printf(RESET_COLOR "\n");
    break;

    case 2: 
    printf(YELLOW "Ves Amarillo");
    printf(RESET_COLOR "\n");
    break;

    case 3: 
    printf(GREEN "Ves Verde");
    printf(RESET_COLOR "\n");
    break;
    
    case 4: 
    printf(RED "Ves Rojo");
    printf(RESET_COLOR "\n");
    break;

    case 5: 
    printf(PURPLE "Ves Morado");
    printf(RESET_COLOR "\n");
    break;

    case 6: 
    printf(ORANGE "Ves Naranja");
    printf(RESET_COLOR "\n");
    break;
    
    case 7: 
    printf(WHITE "Ves Blanco");
    printf(RESET_COLOR "\n");
    break;
    }


    return EXIT_SUCCESS;
}
