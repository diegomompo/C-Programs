#include <stdio.h>
#include <stdlib.h>


int main(){

    int horas;
    double v_hora, s_dia = 0, s_semana = 0, s_mes = 0, s_ano = 0;


    // ENTRADA

    scanf("%d", &horas);
    scanf("%lf", &v_hora);
    
    // CÁLCULOS

    for(int i=1; i<=horas; i++)
        s_dia += v_hora; 
   
    for(int i=1; i<=horas*5; i++)
        s_semana += v_hora; 
    
    for(int i=1; i<=(horas*5)*4; i++)
        s_mes += v_hora; 
    
    for(int i=1; i<=((horas*5)*4)*12; i++)
        s_ano += v_hora; 
    

    // SALIDA

    printf("%.2lf euros/dia\n", s_dia);
    printf("%.2lf euros/semana\n", s_semana);
    printf("%.2lf euros/mes\n", s_mes);
    printf("%.2lf euros/año\n", s_ano);

    return EXIT_SUCCESS;
}
