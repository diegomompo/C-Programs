
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define OUT "caracter.txt"
#define fINFORMA fprintf
#define N 0x100

char * program_name;

void print_usage(int exit_code){

  ("Número", program_name);
    exit(exit_code);
}


char entrada(char buffer[N], int argc, char *argv[]){

    if(argc < 1)
        return EXIT_FAILURE;

    strcpy(buffer, argv[1]);
    
    return buffer[N];

}

int main(int argc, char *argv[]){

    FILE *res;
    char buffer[N];
    char *pf;
   

    program_name = argv[0];

    /*ENTRADA*/ buffer[N] = entrada(buffer, argc, argv);

    pf = buffer;

    while(*pf != '\0'){
      if(*pf == 'i') 
          *pf = '!';
      pf++;
    }

    if(!(res = fopen (OUT, "w")))
        return EXIT_FAILURE;
    fINFORMA(res, "%s\n", buffer);
    fclose(res);


    return EXIT_SUCCESS;
}
