#include <stdio.h>
#include <stdlib.h>

int main(){

    int n_cel;
    int n_fah;

    // ENTRADA
    
    scanf("%d", &n_cel);

    
    // CÁLCULOS
    
    n_fah = (n_cel * 1.8)+32;
    
    // SALIDA
    
    printf("%d\n", n_fah);

    return EXIT_SUCCESS;
}
