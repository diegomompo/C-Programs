#include <stdio.h>
#include <stdlib.h>

#define OUT "palabra.txt"
#define FINFORMA fprintf
#define MAX_LIN 0X1000

int entrada(int numero, int argc, char *argv[]){

    if(argc < 2)
        return EXIT_FAILURE;

    numero = atoi(argv[1]);
    
    return numero;

}

int main(int argc, char *argv[]){

    FILE *pf;
    int numero;
    char buffer[MAX_LIN];


    if(!(pf = fopen (OUT, "w"))){
        fprintf(stderr, "No he podido abrir %s\n", OUT);
        return EXIT_FAILURE;

    }

    if(argc < 1)
        return EXIT_FAILURE;

    /*ENTRADA*/ numero = entrada(numero, argc, argv);

    for(int i=0; i<numero; i++){
        printf("Frase: ");
        fgets(buffer, MAX_LIN, stdin);
        fprintf(pf, buffer);
    }

    fclose(pf);

    return EXIT_SUCCESS;
}
