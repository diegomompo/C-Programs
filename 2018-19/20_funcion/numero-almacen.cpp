#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 10
#define C 2

int cuadrado(int i){

    return pow(i, C);
}
void rellena(int a[N]){

    for(int i = 0; i<N; i++)
        a[i] = cuadrado(i);
}

int main(){

    int a[N];
    
    rellena(a);

    for(int i = 0; i<N; i++)
    printf("%d\n", a[i]);

    
    return EXIT_SUCCESS;
}
