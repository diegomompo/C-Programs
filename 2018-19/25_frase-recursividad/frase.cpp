#include <stdio.h>
#include <stdlib.h>

char imprimir_todo(char *f){

    if(f == "\0")
        return printf("\n");

    return f[0] + imprimir_todo(f+1);
}


int main(){

    char frase[]="Dabale arroz a la zorra el abad \0";
    char *f = frase;
    char res;

    /*CÁLCULOS*/

    printf(" %c", *f);
    
    /*SALIDA*/

    printf(" %c", imprimir_todo(f));

    return EXIT_SUCCESS;
}
