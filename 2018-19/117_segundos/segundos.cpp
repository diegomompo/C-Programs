#include <stdio.h>
#include <stdlib.h>

#define HS 3600
#define MS 60
#define HMS 3

double calculo(int hora[HMS], int seg_hora, int seg_minute, int resultado){

    seg_hora = hora[0] * HS; 
    seg_minute = hora[1] * MS;

    resultado = seg_hora + seg_minute + hora[2];

    return resultado;

}

int main(){

    int hora[HMS];
    int seg_hora;
    int seg_minute;
    int resultado;

    // ENTRADA

    for(int i=0; i<HMS; i++)
     scanf("%d", &hora[i]);
   
    /*CALCULO*/ 
    
    resultado = calculo(hora, seg_hora, seg_minute, resultado); 
    
    // SALIDA
    
    printf("%d\n", resultado);

    return EXIT_SUCCESS;
}
