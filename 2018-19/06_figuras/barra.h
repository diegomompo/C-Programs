#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "define_color.h"
#include "math.h"


#define N     100
#define DELAY 100000


int cargando(int opcion){

     struct variable;
   
  for (int veces=0; veces<N; veces++){
    for (int columna=0; columna<veces; columna++)
      fprintf(stderr, BLUE"=");
    fprintf(stderr, BLUE "Cargando figura %i%%\r",veces);
    usleep(DELAY);
  }
  printf(RESET_COLOR "\n");
}

// -----------------------------------------------------------------------------
int calculando(int opcion){

      struct variable;

  for (int veces=0; veces<N; veces++){
    for (int columna=0; columna<veces; columna++)
      fprintf(stderr, BLUE "=");
    fprintf(stderr, BLUE "Calculando área de la figura %i%%\r",veces);
    usleep(DELAY);
  }
  printf(RESET_COLOR "\n");
}
