#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define OUT "degrees.dat"
#define F1 1.8
#define F2 32
#define K1 273.15
#define N 1
#define fINFORM fprintf

char * program_name;
void print_usage(FILE *outstream, int exit_code){

    fprintf(outstream,
    "This is a program that covert degrees Celsius to Fahrenheit or Kelvin. - \n\n"
    "Usage:\n"
     "-h <help> \t h: Help the program\n\n"
     "-i <number> \t i: Introduce a number\n"
     "-f <number> \t f: Calculated degrees Celsius to Fahrenheit\n"
     "-k <number> \t k: Calculated degrees Celsius to Kelvin\n"
     "Example %s -i <number> -f or -k \n\n\n"
     "Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}
int entry(int number, int argc, char *argv[]){

    if(argc < 0)
        return EXIT_FAILURE;

    number = atoi(argv[2]);

    return number;
}
int fah(int number, FILE *pf){
    
    int F;
    F = F1*number + F2;
    fwrite(&F, sizeof F, N, pf);
}
int kel(int number, FILE *pf){

    int K;
    K = number + K1;

    fwrite(&K, sizeof K, N, pf);
}

int main(int argc, char *argv[]){

    int number;
    int c;
    FILE *pf;
    program_name = argv[0];

    if(!(pf = fopen(OUT, "wb"))){
        fINFORM(stderr, "Can't file open %s", OUT);
        return EXIT_FAILURE;
    }
    while((c = getopt (argc, argv, "h:ifk")) != -1){

        switch(c){
          
            case 'h':
              print_usage(stdout, 0);
              break;
            case 'i':
              /*ENTRY*/ number = entry(number, argc, argv);
              break;
            case 'f':
              /*CALCULATED AND RETURN*/ fah(number, pf);
              break;
            case 'k':
              /*CALCULATED AND RETURN*/ kel(number, pf);
              break;
            case '?':
              if(optopt != 'i' && optopt != 'f' && optopt != 'k')
                  fINFORM(stderr, "Invalid option\n");
              else
                  fINFORM(stderr, "Option argument missing\n");

              print_usage(stderr, 1);
              break;
            default:
              abort();
              break;
      }
    } 

    fclose(pf);  


    return EXIT_SUCCESS;
}
