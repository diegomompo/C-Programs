#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include "define_color.h"

#define N 1
#define O 11
#define B 100
#define R 99
#define DELAY 50000

enum Option {

    Start,
    Exit,
    OPTIONS
};

const char *text[] = {

    "Start",
    "Exit",
};

int sav[N];

void push(int number){
    
    static int i=0;
    sav[i] = number;
}

int ask_number(int number){

    printf("Introduce the number: ");
    scanf("%i", &number);
    push(number);
}

int check(int sav[N], int random) {

    if(sav[0] > random)
        printf("The number %i is older than the number random\n", sav[0]);
    else if(sav[0] < random)
        printf("The number %i is less than the number random\n", sav[0]);
    else
        printf("\n");
}
int oppor(int sav[N], int random){

  int opor = O;

   if(sav[0] != random){
  
      opor = opor - N;
      printf("Reamining opportunities: %i\n", opor);

   } 
}

void title(){

    system("clear");
    system ("toilet -ffuture --metal GUESS THE NUMBER" );
    printf("\n");
}

enum Option menu(){

    int option;

    do{

    title();

    printf("\t\tOPTIONS\n");
    printf(CYAN "\t================================\n" RESET_COLOR);

    for(int i=0; i<OPTIONS; i++)
        printf(CYAN "\t\t%i. " RESET_COLOR "%s\n\n", i+1, text[i]);

    printf(CYAN "\t\tOption: " RESET_COLOR);
    scanf("%i", &option);
    
    option--;
    }while(option<Start || option>OPTIONS);

    return (enum Option) option;
}
void generating(enum Option op){

    title();

    struct variable;

    for(int times = 0; times<B; times++){
        for(int columns=0; columns<times; columns++)
            fprintf(stderr, GREEN_CLARO "=");
        fprintf(stderr, GREEN_CLARO "Generate the number %i%%\r", times);
        usleep(DELAY);
    }
    printf(RESET_COLOR);
}
void checking(enum Option op){

    title();

    struct variable;

    for(int times = 0; times<B; times++){
        for(int columns=0; columns<times; columns++)
            fprintf(stderr, GREEN_CLARO "=");
        fprintf(stderr, GREEN_CLARO "Checking if both number are the same %i%%\r", times);
        usleep(DELAY);
    }
    printf(RESET_COLOR);
}

int main(int argc, char *argv[]){

    enum Option op;
    int number;
    int random;
    int opor = O;
    int (*introduce)(int){ &ask_number };
    int (*check2)(int[N], int){ &check };
    int (*opportunity)(int[N], int){ &oppor};

    srand(time(NULL));

    /* MENU */ op = menu();

    /* GENERATE THE NUMBER */ 
    random = rand()%R;
    generating(op);

    do{

    title();

    /* CHECK IF THE NUMBER IS OLDER OR LESS THAN THE NUMBER RANDOM */ (*check2)(sav, random);

    /* REMAINING OPPORTUNITIES */ (*opportunity)(sav, random);

    /* INTRODUCE THE NUMBER */ (*introduce)(number);

    /* CHECK IF THE NUMBER IS THE SAME AS THE NUMBER RANDOM */ checking(op);

    }while(sav[0] != random);

    if(opor != 0)
        printf("Congratulations!!!. You are win\n");
    else
        printf("You don't have more opportunities. The number was %i\n", sav[0]);


    return EXIT_SUCCESS;
}
