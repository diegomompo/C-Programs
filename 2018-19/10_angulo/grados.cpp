#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifdef GRADOS
#define K 1
const char *unidad ="º";
#else
#define K M_PI / 180
const char *unidad = "rad";
#endif

double arad(double ang){

#ifdef GRADOS
    return M_PI * ang / 180
#else
    return ang;
#endif
}

int main(){

     for(double ang = 0; ang<K*360; ang+=K*.5)
        printf("%.2lf%s:\tcos(%.2lf) = %.6lf\n", ang, unidad, ang, cos( arad(ang) ));

    return EXIT_SUCCESS;
}


