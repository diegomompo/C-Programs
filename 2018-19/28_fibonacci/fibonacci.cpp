#include <stdio.h>
#include <stdlib.h>
#define fINFORMA fprintf
#define OUT "fibonacci.dat"
#define N 10

int rellenar(int fibo[N], int i){
  if(i == 1){
    fibo[0] = 1;
    return fibo[1] == 1;
  }

  return fibo[i] = rellenar (fibo, i-1) + fibo[i-2];
}
int main(int argc, char *argv[]){

    int fibo[N];
    int n = sizeof(fibo) / sizeof(int);
    FILE *pf;

    rellenar(fibo, N-1);
    /*SALIDA*/

    if(!(pf = fopen (OUT, "wb"))){
      fINFORMA(stderr, "No se pudo abrir el fichero %s\n", OUT);
      return EXIT_FAILURE;
    }

      fwrite(fibo, sizeof(int), n, pf);

    fclose(pf);

    return EXIT_SUCCESS;
}
