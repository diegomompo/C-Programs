#include <stdio.h>
#include <stdlib.h>

#define N 10

int main(){

    unsigned lista[N];

    for(int i = 0; i < N; i++)
        printf("lista[%i] = %u\n",i, lista[i]);

    return EXIT_SUCCESS;
}
