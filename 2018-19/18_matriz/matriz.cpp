#include <stdio.h>
#include <stdlib.h>

#define I 3
#define J 3
#define X 0
#define Y 1
#define Z 2
#define D 3
#define DIM 3

int main(){

    double num[3][DIM], determinante = 0, multiplicacion, multiplicacion2, determinante2 = 0, resultado;

    for(int i=0; i<DIM; i++)
     /* ENTRADA */  scanf("%lf,%lf,%lf", &num[i][X], &num[i][Y], &num[i][Z]);

     /* CÁLCULO */

     double matriz[DIM][DIM] = {
     
         {num[0][X], num[0][Y], num[0][Z]},
         {num[1][X], num[1][Y], num[1][Z]},
         {num[2][X], num[2][Y], num[2][Z]}
     };

      for(int f = 0; f<I; f++){
          multiplicacion = 1;
        for(int d=0; d<J; d++)
            multiplicacion *= matriz[(f+d)%DIM][0+d];
        determinante += multiplicacion;  
      }

      for (int f=0; f<D; f++){
          multiplicacion = 1;
      for (int d=0; d<D; d++)
          multiplicacion *= num[(f+d)%D][2-(0+d)];
        determinante -= multiplicacion;
     }


    

    /* SALIDA */ printf("%lf\n", determinante);

    return EXIT_SUCCESS;
}

// double matriz[3][3] = {
// {1,3,2}
// {7,5,4}
// {4,8,2}
// };
//
