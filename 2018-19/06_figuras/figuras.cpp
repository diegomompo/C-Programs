#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "define_color.h"
#include "math.h"
#include "barra.h"
#include "define_figuras.h"
#include "menu.h"

const char* figura[] = {
   "TRIÁNGULO",
   "RECTÁNGULO",
   "CUADRADO",
   "CÍRCULO"
};

// -----------------------------------------------------------------------------
int main(){ // FUNCIÓN GENERAL

    int opcion;
    int resultado;

    system("clear");
    printf(RED "\tFIGURAS");
    printf(RESET_COLOR "\n");

    menu();

    scanf("%i", &opcion);
 
    printf(RESET_COLOR "\n");

    switch(opcion){
    
    case triangulo:
      system("clear");
      printf(YELLOW "%s\n", figura[opcion]);
      printf(RESET_COLOR "\n");
      cargando(opcion);
      funcion_triangulo();
      calculando(opcion);
    break;
    case rectangulo:
      system("clear");
      printf(YELLOW "%s\n", figura[opcion]);
      printf(RESET_COLOR "\n");
      cargando(opcion);
      funcion_rectangulo();
      calculando(opcion);
    break;
    case cuadrado:
      system("clear");
      printf(YELLOW "%s\n", figura[opcion]);
      printf(RESET_COLOR "\n");
      cargando(opcion);
      funcion_cuadrado();
      calculando(opcion);
    break;
    case circulo:
      system("clear");
      printf(YELLOW "%s\n", figura[opcion]);
      printf(RESET_COLOR "\n");
      cargando(opcion);
      funcion_circulo();
      calculando(opcion);
    break;
    default:
      printf("No sé cuál es la opción: (%i)\n", opcion + 1);
      return EXIT_FAILURE;
    break;
    }

    return EXIT_SUCCESS;
}
