#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define OUT "dict/diccionario_esp.txt"
#define N 0x100
#define L 6

int main(int argc, char *argv[]){

    char word;
    int ale;
    char palabra[N];
    int c;
    int cont_words = 0;
    int len;
    int cont = 0;
    char res[len];
    char letter;
    int opor;
    int oportunidades;
    char letraerror[cont];
    bool existe = false;
    bool acierto = false;
    bool game_over = false;
    FILE *pf;

    printf("Palabra: ");
    scanf("%s", &word);

    strcpy(palabra, &word);

    len = strlen(&word);
    
    for(int i=0; i<len; i++){
    palabra[i] = '_';
    }

    printf("\n");

    do{  
        do{
            /*IMPRIMIR*/
            if(opor < L){
                printf("Letras erroneas: ");
                for(int i=0; i<strlen(letraerror); i++)
                    printf(" %c", letraerror[i]);
            }
            printf("\n");
            for(int i=0; i<len; i++)
                printf(" %c", res[i]);

            printf("Letter: ");
            scanf("%c", &letter);

            /*EXISTE*/
            
            for(int i=0;i<strlen(letraerror);i++){

	        if(letter==letraerror[i]) {

                    printf("Ya la escribistes!.\n");
                    existe = true;
                }
            }
        }while(existe);

        /* LA LETRA ESTÁ DENTRO DE LA PALABRA*/

        for(int i=0; i<len;  i++){
              if(letter=palabra[i]){
                    palabra[i] = letter;
                    acierto = true;
              }
        }

        if(acierto){
            if(word != 0){
              game_over = false;
            }
            else{
              game_over = true;
            }
        }
        else{
            letraerror[cont++] = letter;
            --oportunidades;
        }
    }while(oportunidades>0&&game_over==false);

    if(game_over&&oportunidades > 4){
      printf("EXCELENTE TRABAJO!\n");
    }
    else if(game_over&&oportunidades > 2){
      printf("MUY BUEN TRABAJO!\n");
    }
    else if(oportunidades > 2){
        printf("BUEN TRABAJO!\n");
    }
    else{
      printf("\n\tAHORCADO, JEJEJE!\n\n\t");

      printf("La palabra era...\n\n\t");

      printf(" %c", word);
    }


// 1º Introducimos la palabra
// 2º Imprimimos en pantalla los caracteres de la palabra

    return EXIT_SUCCESS;
}

