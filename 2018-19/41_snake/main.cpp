#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "snake.h"

void pintar (struct TSnake snake) {
    clear();// falta una cosa.
    for(int i = 0; i<snake.cima; i++)
      mvprintw ( snake.anillo[i].pos.y, 
                 snake.anillo[i].pos.x, "O" );
    refresh ();
}

int  main(int argc, char *argv[]){
    struct TSnake snake;
    int input;

    initscr ();
    halfdelay (2);
    keypad (stdscr, TRUE);
    curs_set(0);
    iniciar (LINES, COLS);

    parir(&snake);

    do {
        input = getch ();

        if(input >= KEY_DOWN && input <= KEY_RIGHT)
            snake.anillo[0].vel = velocidades[input - KEY_DOWN];

        /*switch(input) {
            case KEY_UP:
                snake.anillo[0].vel.x= 0;
                snake.anillo[0].vel.y= -1;
                break;
            case KEY_DOWN:
                snake.anillo[0].vel.x= 0;
                snake.anillo[0].vel.y= 1;
                break;

            case KEY_RIGHT:
                snake.anillo[0].vel.x= 1;
                snake.anillo[0].vel.y= 0;

                break;

            case KEY_LEFT:
                snake.anillo[0].vel.x= -1;
                snake.anillo[0].vel.y= 0;

                break;
        }*/


        mover (&snake);

        if(snake.anillo[0].pos.x > COLS){

            printf("\n");
            system("zenity --info --title 'SNAKE' --text 'GAME OVER'");
            return EXIT_SUCCESS;
        }

     
        pintar (snake);
    } while ( input != 0x1B );

    endwin ();
    curs_set(1);

    return EXIT_SUCCESS;
}
