#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include "define_color.h"

enum Operaciones menu (){ 
   
      int op; 
   
          do{ 
    
         printf("\t\t OPERACIONES\n"); 
         printf(YELLOW "\t\t=====================================\n" RESET_COLOR); 
         for(int i=0; i<OPERACIONES; i++) 
             printf(CYAN "\t\t%i. " RESET_COLOR "%s\n", i, texto[i]); 
     
         printf(CYAN "\t\t Operacion a realizar: " RESET_COLOR); 
         scanf("%d", &op); 
         __fpurge(stdin); 

        }while(op<suma || op >= OPERACIONES); 

        return (enum Operaciones) op; 
}

