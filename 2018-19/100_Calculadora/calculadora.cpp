#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "suma.h"  // Librerías que he creado yo
#include "restar.h"
#include "multiplica.h"
#include "divide.h"
#include "potencia.h"
#include "raiz.h"
#include "hipotenusa.h"


int main(){

   // Declaramos las variables	
   int opcion;
   int resultado;

   // Menú para escoger la opción
   printf("**********************************\n");
   printf("***** 1. Sumar         ***********\n");
   printf("***** 2. Restar        ***********\n");
   printf("***** 3. Multiplicar   ***********\n");
   printf("***** 4. Dividir       ***********\n");
   printf("***** 5. Potencia      ***********\n");
   printf("***** 6. Raíz cuadrada ***********\n");
   printf("***** 7. Hipotenusa    ***********\n");
   printf("***** 8. Salir         ***********\n");
   printf("**********************************\n");

   printf("\n");

   printf("Elige una opción: ");
   scanf("%i", &opcion);


   // VERSION SWITCH CASE
 
   switch(opcion){
    case 1:
        suma();
	break;
    case 2:
        resta();
        break;
    case 3:
        multiplica();    
        break;
   case 4:
       divide();     
       break;      
   case 5:
       potencia();
       break;
   case 6:
       raiz();
       break;
   case 7:
       hipotenusa();
       break;
   case 8:
       printf("¡¡¡HASTA LUEGO!!!");
       break;
	
   }

//-------------------------------------------------------------------------------------------

    // VERSIÓN TERNARIO
  
  /*  printf( 
    (opcion == 1) ? suma():void
    (opcion == 2)? resta():void
    (opcion == 3)? multiplica():voi
    (opcion == 4)? divide():void
    (opcion == 5)? potencia():void
    (opcion == 6)? raiz():void
    (opcion == 7)? salir:void); */


  return EXIT_SUCCESS;
}
