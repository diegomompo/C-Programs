#include <allegro5/allegro.h>

void inicio(){
  
    allegro init();
    set_color_depth(16);
    set_gfx_mode(GFX_AUTODETECT, 480, 480, 0, 0);

    install_keyboard();
    install_mouse();
    install_timer();
}
void fin(){
  clear_keybuf();
  allegro_exit();
}
int main(int argc, char *argv[]){

    inicio();

    tetxout(screen, font, "¡¡¡HELLO WORLD!!!", 100, 0, pallete_color(15), 0);

    readkey();

    fin();

    return EXIT_SUCCESS;
}
