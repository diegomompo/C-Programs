#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define OUT "degrees.dat"
#define INFORM printf


int main(int argc, char *argv[]){

    int number;
    FILE *pf;

    if(!(pf = fopen(OUT, "rb"))){
        fprintf(stderr, "Can't file open %s", OUT);
        return EXIT_FAILURE;
    }
    
    fread(&number, sizeof(int), 1, pf);
    printf(" Number: %i\n", number);

    fclose(pf);  


    return EXIT_SUCCESS;
}
