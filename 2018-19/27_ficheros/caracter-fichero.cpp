#include <stdio.h>
#include <stdlib.h>

#define FILENAME "nombre.txt"

int main(int argc, char *argv[]){

   FILE *pf;
   long int inicio, fin, distancia;

   if(!(pf = fopen(FILENAME, "r"))){
       fprintf(stderr, "No se ha podido leer el fichero\n");
       return EXIT_FAILURE;
    }
  
   inicio = ftell(pf); 
   fseek(pf, 0, SEEK_END);
   fin = ftell(pf);
   distancia = fin - inicio;
   printf("Inicio %li bytes\n"
          "Fin: %li bytes\n"
          "Distancia: %li bytes\n", inicio, fin, distancia);
   fclose(pf);

    return EXIT_SUCCESS;
}
