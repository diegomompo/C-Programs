#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "define_color.h"
#include "math.h"


#define N     100
#define DELAY 50000


int cargando(){

     struct variable;
   
  for (int veces=0; veces<N; veces++){
    for (int columna=0; columna<veces; columna++)
      fprintf(stderr, YELLOW "=");
    fprintf(stderr, YELLOW "cargando opción. Por favor, espere.%i%%\r",veces);
    usleep(DELAY);
  }
  printf(RESET_COLOR "\n");
}
//--------------------------------------------------------------------------

int consultando(){

      struct variable;

  for (int veces=0; veces<N; veces++){
    for (int columna=0; columna<veces; columna++)
      fprintf(stderr, YELLOW "=");
    fprintf(stderr, YELLOW "Consultando dinero. Por favor, espere %i%%\r",veces);
    usleep(DELAY);
  }
  printf(RESET_COLOR "\n");
}
// -----------------------------------------------------------------------------
int ingresando(){

      struct variable;

  for (int veces=0; veces<N; veces++){
    for (int columna=0; columna<veces; columna++)
      fprintf(stderr, YELLOW "=");
    fprintf(stderr, YELLOW "Ingresando dinero. Por favor, espere %i%%\r",veces);
    usleep(DELAY);
  }
  printf(RESET_COLOR "\n");
}

// ---------------------------------------------------------------------------------

int retirando(){

      struct variable;

  for (int veces=0; veces<N; veces++){
    for (int columna=0; columna<veces; columna++)
      fprintf(stderr, YELLOW "=");
    fprintf(stderr, YELLOW "Retirando dinero. Por favor, espere %i%%\r",veces);
    usleep(DELAY);
  }
  printf(RESET_COLOR "\n");
}

//-----------------------------------------------------------------------------------

int saliendo(){

      struct variable;

  for (int veces=0; veces<N; veces++){
    for (int columna=0; columna<veces; columna++)
      fprintf(stderr, YELLOW "=");
    fprintf(stderr, YELLOW "Saliendo. Por favor, espere %i%%\r",veces);
    usleep(DELAY);
  }
  printf(RESET_COLOR "\n");
}
