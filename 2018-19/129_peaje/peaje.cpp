#include <stdio.h>
#include <stdlib.h>
#include "barra.h"
#include "define_color.h"
#include "menu.h"

#define V 3
#define L 21

enum{

    turismo,
    autobus,
    moto
};

int main(){
    
    int veh;
    
    // ENTRADA

    system("clear");
    printf(GREEN "\tPEAJE");
    printf(RESET_COLOR "\n");

    menu();
    scanf("%d", &veh);

    cargando();
    
    // CÁLCULOS

    switch(veh){

        case turismo:
            calculando();
            /* SALIDA */ printf("Tu peaje es de $500\n");
        break;
        case autobus:
            calculando();
            /* SALIDA */ printf("Tu peaje es de $3000\n");
        break;
        case moto:
            calculando();
            /* SALIDA */ printf("Tu peaje es de $200\n");
        break;
        default:
           /* SALIDA */ printf("Error. Vehículo no autorizado\n");
        break;
    }

    return EXIT_SUCCESS;
}
