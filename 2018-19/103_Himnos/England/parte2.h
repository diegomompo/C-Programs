#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 1
#define MEDIO 500000
#define UNO 1000000
#define UNO_MEDIO 1500000
#define DOS_MEDIO 2500000

int Parte2_ENG(){

    for(int j=0; j<VECES; j++){
       
	fprintf(stderr, "\a");
	usleep(UNO_MEDIO);
	fprintf(stderr, "\a");
	usleep(MEDIO);
	fprintf(stderr, "\a");
	usleep(UNO);
	fprintf(stderr, "\a");
	usleep(UNO);
	fprintf(stderr, "\a");
        usleep(UNO);
	fprintf(stderr, "\a");
        usleep(UNO);
	fprintf(stderr, "\a");
	usleep(UNO_MEDIO);
	fprintf(stderr, "\a");
	usleep(MEDIO);
	fprintf(stderr, "\a");
	usleep(UNO);
	fprintf(stderr, "\a");
	usleep(UNO);
	fprintf(stderr, "\a");
	usleep(MEDIO);
	fprintf(stderr, "\a");
        usleep(MEDIO);
	fprintf(stderr, "\a");
        usleep(MEDIO);
	fprintf(stderr, "\a");
        usleep(MEDIO);
	fprintf(stderr, "\a");
	usleep(UNO_MEDIO);
	fprintf(stderr, "\a");
	usleep(MEDIO);
	fprintf(stderr, "\a");
	usleep(UNO);
	fprintf(stderr, "\a");
	usleep(MEDIO);
	fprintf(stderr, "\a");
	usleep(MEDIO);
	fprintf(stderr, "\a");
	usleep(UNO);
	fprintf(stderr, "\a");
	usleep(UNO);
	fprintf(stderr, "\a");
	usleep(DOS_MEDIO);

    }

    return EXIT_SUCCESS;
}
