#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void Decimal_Hexadecimal(){
   
    int dec_hex;

    printf("Ingrese un número para convertirlo en Hexadecimal: ");
    scanf("%d", &dec_hex);

    printf("\n");

    printf("EL número en Hexadecimal es 0x%X\n", dec_hex);
}

void Decimal_Octal(){
    
    int dec_oct;

    printf("Ingrese un número para convertirlo en Octal: ");
    scanf("%d", &dec_oct);

    printf("\n");

    printf("El número convertido en Octal es: 0x%o\n", dec_oct);

 }

void Decimal_Binario(){
	
    int dec_bin;


    printf("Ingrese un número para convertirlo en Binario: ");
    scanf("%d", &dec_bin);

    int binario = 0;
    int posicion = 1;

    while(dec_bin > 0){
 	binario = binario + (dec_bin%2)*posicion;
	dec_bin /=2;
	posicion *=10;

   }	

	printf("El número  convertido en Binario es %d\n", binario);

    

}

