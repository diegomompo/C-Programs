#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 1
#define CUARTO 250000
#define MEDIO 500000
#define UNO 1000000
#define UNO_MEDIO 1500000
#define DOS 2000000

int Parte2_GER(){

     int i = 0;

     while(i < VECES){

      fputc('\a', stderr);
      usleep(UNO);
      fputc('\a', stderr);
      usleep(UNO);
      fputc('\a', stderr);
      usleep(MEDIO);
      fputc('\a', stderr);
      usleep(MEDIO);
      fputc('\a', stderr);
      usleep(UNO);
      fputc('\a', stderr);
      usleep(UNO);
      fputc('\a', stderr);
      usleep(UNO);
      fputc('\a', stderr);
      usleep(MEDIO);
      fputc('\a', stderr);
      usleep(MEDIO);
      fputc('\a', stderr);
      usleep(UNO);
      fputc('\a', stderr);
      usleep(UNO);
      fputc('\a', stderr);
      usleep(UNO);
      fputc('\a', stderr);
      usleep(UNO_MEDIO);
      fputc('\a', stderr);
      usleep(MEDIO);
      fputc('\a', stderr);
      usleep(UNO);
      fputc('\a', stderr);
      usleep(MEDIO);
      fputc('\a', stderr);
      usleep(MEDIO);
      fputc('\a', stderr);
      usleep(DOS);

      i++;

      }

    return EXIT_SUCCESS;
}
