#include <stdio.h>
#include <stdlib.h>

int  potencia(){

int base;
int exponente;

printf("Introduce la base: ");
scanf("%i", &base);
printf("Introduce el exponente: ");
scanf("%i", &exponente);

int res_potencia = pow(base, exponente);

printf("El resultado de la base %i con exponente %i es %i \n", base, exponente, res_potencia);

}
