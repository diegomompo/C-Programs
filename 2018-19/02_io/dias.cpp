#include <stdio.h>
#include <stdlib.h>

#define total_dias 365

int main(){

    int dias;
    int edad;
    int ano_actual;
    int ano_nacimiento;
    int mes_nacimiento;
    int mes_actual;
    int mes;
    int dias_vivido;

    printf("Dime tu año de nacimiento: ");
    scanf("%i", &ano_nacimiento);

    printf("Dime tu año actual: ");
    scanf("%i", &ano_actual);

    edad = ano_actual - ano_nacimiento;

    printf("Dime tu mes de nacimiento: ");
    scanf("%i", &mes_nacimiento);   
    
    printf("Dime tu mes actual: ");
    scanf("%i", &mes_actual);     
    
    mes = mes_actual - mes_nacimiento;

    dias = edad * 365;

    printf ("Usted ha vivido %i dias\n", dias);

    dias_vivido = mes * 365;

    printf("Usted ha vivido en total  %i dias\n", dias_vivido);
    


    return EXIT_SUCCESS;
}
