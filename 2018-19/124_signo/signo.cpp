#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(){

    char nombre[20];
    char signo[20];
    char salida;

    // ENTRADA

    scanf("%s", nombre);
    scanf("%s", signo);
    
    // CÁLCULOS

    // VERSION INTENTO
    // printf((strcmp (signo,"Aries")==0)? "%s", nombre : "No es un signo Aries");
    
    // VERSION REAL

    if(strcmp(signo,"Aries")==0){

        printf("%s\n", nombre);
    }else{
        printf("No es un signo Aries\n");
    }

    return EXIT_SUCCESS;
}
