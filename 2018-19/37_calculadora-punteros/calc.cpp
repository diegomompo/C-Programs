#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "define_color.h"
#include "math.h"
#define N 2
#define B 100
#define DELAY 50000

enum Opcion {
  suma,
  resta,
  multiplicacion,
  division,
  OPCIONES
};

const char *texto[] = {
  "sumar",
  "restar",
  "multiplicar",
  "dividir"
};

const char simb[] = "+-*/";

double sum(double op1, double op2) { return op1 + op2; }
double res(double op1, double op2) { return op1 - op2; }
double mul(double op1, double op2) { return op1 * op2; }
double div(double op1, double op2) { return op1 / op2; }

double sav[N];

void push(double num){
      static int i = 0;
      sav[i++] = num;
}

void titulo(){

    system("clear");
    system ("toilet -ffuture --metal CALCULADORA" );
    printf("\n");
}

enum Opcion menu() {
    int opcion;

    do{
    
    titulo();

    printf("\t\tOPCIONES\n");
    printf(YELLOW "\t==========================\n" RESET_COLOR);
    for(int i = 0; i<OPCIONES; i++)
      printf(CYAN "\t\t%i. " RESET_COLOR "%s\n", i+1, texto[i]);
    
    printf(CYAN "\t\tOpción: " RESET_COLOR);
    scanf("%i", &opcion);
    opcion--;
    }while(opcion<suma || opcion >= OPCIONES);

    return (enum Opcion) opcion;
}

void cargando(enum Opcion op){
  
    titulo();

    struct variable;
    
    for(int veces = 0; veces <= B; veces++){
      for(int columna = 0; columna < veces; columna++)
          fprintf(stderr, GREEN_CLARO "=");
      fprintf(stderr, GREEN_CLARO "Cargando operandos para %s %i%%\r", texto[op], veces);
      usleep(DELAY);
    }
    printf(RESET_COLOR);
}

void pide_op(double num){

    titulo();

    for(int i=0; i<N; i++){
      printf("\t\tOperando %i: ", i+1);
      scanf("%lf", &num);
      push(num);
    }
}

void calculando(){

  titulo();
  struct variable;

  for(int veces = 0; veces <= B; veces++){
    for(int columna = 0; columna < veces; columna++)
      fprintf(stderr, GREEN_CLARO "=");
    fprintf(stderr, GREEN_CLARO "Calculando operación %i%%\r", veces);
    usleep(DELAY);
  }
  printf(RESET_COLOR);

}

int main(int argc, char *argv[]){

    enum Opcion op;
    double num;
    double (*operaciones[]) (double, double) {
      &sum, &res, &mul, &div };

    /*MENU*/
    op = menu();

    /* CARGANDO OPERANDOS PARA LA OPERACIÓN CORRESPONDIENTE */

    cargando(op);

    /*PIDE NUMEROS*/

    pide_op(num);

    /* CALCULANDO OPERACION */

    calculando();

    /*SALIDA*/

    titulo();

    printf("%s (%lf %c %lf) = %lf\n",
          texto[op], 
          sav[0], simb[op], sav[1],
          (*operaciones[op])(sav[0], sav[1]));

    return EXIT_SUCCESS;
}
