#include <stdio.h>
#include <stdlib.h>
#include "barra.h"
#define INFORMA printf

char * program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
     Usage: %s <numero>\n\n\
     Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}
int entrada(int numero, int argc, char *argv[]){

    if(argc < 1)
        print_usage (1);

    numero = atoi(argv[1]);

    return numero;
}
int arbol(int numero){

    cargando();


    for(int i=0; i<numero; i++){
        for(int j=0; j<=i; j++){
            INFORMA("*");
        }
        printf("\n");
    }
}

int main(int argc, char *argv[]){

    int numero;
    program_name = argv[0];

    /*ENTRADA*/ numero = entrada(numero, argc, argv);
    
    /*CÁLCULOS*/ arbol(numero);

    return EXIT_SUCCESS;
}
