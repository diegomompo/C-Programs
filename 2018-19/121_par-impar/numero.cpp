#include <stdio.h>
#include <stdlib.h>

int main(){

    int numero;

    // ENTRADA

    scanf("%d", &numero);
    
    // CÁLCULOS
    
    printf((numero%2==0)? "PAR\n": "IMPAR\n");
    
    // SALIDA

    return EXIT_SUCCESS;
}
