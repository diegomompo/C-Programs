#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "Spain/parte1.h"
#include "Spain/parte2.h"
#include "England/parte1.h"
#include "England/parte2.h"
#include "Germany/parte1.h"
#include "Germany/parte2.h"
#include "Germany/parte3.h"
#include "La_Decima/parte1.h"
#include "La_Decima/parte2.h"
#include "La_Decima/parte3.h"

// #define MAX 0x100
// #define MAX 0x100

/* void pon_titulo(int himno){

   char titulo[MAX];
   sprintf("HIMNO NÚMERO %X\n", himno);
   system(titulo);
}
*/
int main(){
  	
    int opcion;
    // char titulo[MAX];

    system("clear");
    system("toilet -fpagga \"HIMNOS\"");

    printf("---------------------------------------------\n");
    printf("-                                           -\n");
    printf("---------- 1. HIMNO DE ESPAÑA     -----------\n");
    printf("-                                           -\n");
    printf("---------- 2. HIMNO DE INGLATERRA -----------\n");
    printf("-                                           -\n");
    printf("---------- 3. HIMNO DE ALEMANIA -------------\n");
    printf("-                                           -\n");
    printf("---------- 4. HIMNO DE LA DÉCIMA ------------\n");
    printf("-                                           -\n");
    printf("---------------------------------------------\n");    
    
    printf("Escoge una opción: ");
    scanf("%X", &opcion);

    // pon_titulo(opcion);


    switch(opcion){
	
      case 1:     
	system("toilet -fpagga \"HIMNO DE ESPAÑA\"");
	Parte1_ESP();
	Parte2_ESP();
      break;

      case 2:      
	system("toilet -fpagga \"HIMNO DE INGLATERRA\"");
	Parte1_ENG();
	Parte2_ENG();
      break;

      case 3:
        system("toilet -fpagga \"HIMNO DE ALEMANIA\"");
        Parte1_GER();
        Parte2_GER();
        Parte3_GER();
      break;

      case 4:
    	system("toilet -fpagga \"HIMNO DE LA DÉCIMA\"");
	Parte1_RMA();
	Parte2_RMA();
	Parte1_RMA();
	Parte2_RMA();
	Parte3_RMA();
      break;
        
      default:       
        printf("¡¡¡OPCIÓN INCORRECTA, HASTA LUEGO!!!\n");
      break;

      }


     return EXIT_SUCCESS;
}

