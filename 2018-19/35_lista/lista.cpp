#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 0x100

int main(int argc, char *argv[]){

    char **lista;
    char opcion;
    char buffer[N];
    int len;
    int i = 0;
    int p = 1;

    do{

      printf("Dime tu nombre: ");
      scanf(" %s", buffer);
      len = strlen (buffer);
      lista = (char**)realloc(lista, p*sizeof(char*));
      lista[i] = (char *) malloc (len+1);
      strncpy (lista[i], buffer, len + 1);

      printf("¿Desea continuar?: ");
      scanf(" %c", &opcion);

      if(opcion == 's'){
         i++;
         p++;
      }
      else
         lista[i+1] = NULL;

   }while(opcion == 's');

    for(char ** palabra = lista; *palabra != NULL; palabra++)
        printf("%s\n", *palabra);

    free(lista);
    
    /*SALIDA*/

    return EXIT_SUCCESS;
}
