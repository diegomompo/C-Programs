#include <stdio.h>
#include <stdlib.h>
#define INFORMA
#define D 2
#define N 0x100

char * program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
     Usage: %s <numero>\n\n\
     Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}
int entrada(int numero, int argc, char *argv[]){
  
    if(argc < 1)
        print_usage (1);

    numero = atoi(argv[1]);

    return numero;

}
int binario(int numero, int i, int A[N]){

    i = 0;

    if(numero!=0){
      A[i] = numero%D;
      printf("%i", A[i]);
    }
    else{
      return printf("\n");
    }

    return binario(numero/2, i+1, A);
}

int main(int argc, char *argv[]){

    int numero;
    int i;
    int A[N];
    program_name = argv[0];

    /*ENTRADA*/ numero = entrada(numero, argc, argv);
    
    /*CÁLCULOS*/ binario(numero, i, A);
    
    /*SALIDA*/

    printf("\n");

    return EXIT_SUCCESS;
}
