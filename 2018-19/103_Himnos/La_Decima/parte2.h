#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 1
#define CUARTO 250000
#define MEDIO 500000
#define UNO 1000000
#define DOS 2000000
#define DOS_MEDIO 2500000

int Parte2_RMA(){

    int i = 0;

    while(i < VECES){
    
     fputc('\a', stderr);
     usleep(UNO);
     fputc('\a', stderr);
     usleep(DOS);
     fputc('\a', stderr);
     usleep(MEDIO);
     fputc('\a', stderr);
     usleep(MEDIO);
     fputc('\a', stderr);
     usleep(UNO);
     fputc('\a', stderr);
     usleep(CUARTO);
     fputc('\a', stderr);
     usleep(DOS);
     fputc('\a', stderr);
     usleep(UNO);
     fputc('\a', stderr);
     usleep(UNO);
     fputc('\a', stderr);
     usleep(DOS);
     fputc('\a', stderr);
     usleep(MEDIO);
     fputc('\a', stderr);
     usleep(MEDIO);
     fputc('\a', stderr);
     usleep(UNO);
     fputc('\a', stderr);
     usleep(CUARTO);
     fputc('\a', stderr);
     usleep(DOS_MEDIO);
     fputc('\a', stderr);
     usleep(UNO);
     fputc('\a', stderr);
     usleep(DOS);
     fputc('\a', stderr);
     usleep(MEDIO);
     fputc('\a', stderr);
     usleep(MEDIO);
     fputc('\a', stderr);
     usleep(UNO);
     fputc('\a', stderr);
     usleep(CUARTO);
     fputc('\a', stderr);
     usleep(DOS);
     fputc('\a', stderr);
     usleep(UNO);
     fputc('\a', stderr);
     usleep(UNO);
     fputc('\a', stderr);
     usleep(DOS);
     fputc('\a', stderr);
     usleep(MEDIO);
     fputc('\a', stderr);
     usleep(MEDIO);
     fputc('\a', stderr);
     usleep(UNO);
     fputc('\a', stderr);
     usleep(CUARTO);
     fputc('\a', stderr);
     usleep(DOS_MEDIO);

    i++;

    }

    return EXIT_SUCCESS;
}
