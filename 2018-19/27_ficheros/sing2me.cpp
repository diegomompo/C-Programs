
#include <stdio.h>
#include <stdlib.h>
#define CANCION "cancion.txt"

void print_usage(){

}

int main(int argc, char *argv[]){

    FILE * pf;
    int c;

    const char * cancion;

    if(argc < 2)
        return EXIT_FAILURE;

    cancion = argv[1];
    
    /*SALIDA*/

    if( !(pf = fopen (CANCION, "r")))
        return EXIT_FAILURE;

    while( (c = getc (pf)) != EOF )
        printf("%c", c);

    fclose(pf);

    return EXIT_SUCCESS;
}
