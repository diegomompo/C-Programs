#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 10 

int main(){

    int numero[N];

    /* Entrada de datos */
    
    /* Cáculos */

    for(int i = 0; i<N; i++){
        numero[i] = pow((i+1),2);
    }

    /* Salida de datos */

   /* for(int i = 0; i<N; i++)
    printf("Número: %d\n", numero[i]); */

    for(int i = 0; i<N ;i++){
        for(int j = 0; j<numero[i]; j++)
        printf("*");
    printf("\n");

    }

    return EXIT_SUCCESS;
}
