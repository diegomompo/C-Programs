#include <stdio.h>
#include <stdlib.h>
#define fINFORMA fprintf
#define OUT "leer_fibonacci.txt"
#define N 9
#define INPUT "fibonacci.dat"

int main(int argc, char *argv[]){

    int datos[N];
    FILE *pf;

    /*SALIDA*/

    if(!(pf = fopen (INPUT, "rb"))){
      fINFORMA(stderr, "No se pudo abrir el fichero %s\n", OUT);
      return EXIT_FAILURE;
    }

      fread(datos, sizeof(int), N, pf);

      fclose(pf);

      for(int i=0; i<N; i++)
          printf("%i ", datos[i]);

      printf("\n");


    return EXIT_SUCCESS;
}
