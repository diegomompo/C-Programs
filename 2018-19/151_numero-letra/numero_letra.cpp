#include <stdio.h>
#include <stdlib.h>

#define fINFORM fprintf
#define OUT "letras.dat"
#define N 10

enum {
  
  zero,
  one,
  two,
  three,
  four,
  five,
  six,
  seven,
  eight,
  nine,
  ten,
};

char * program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
     Usage: %s <numero>\n\n\
     Number: Positive Integer between 1 and 10\n\n", program_name);
    exit(exit_code);
}
int entry(int number, int argc, char *argv[]){

    if(argv < 0)
       return EXIT_FAILURE; 

    number = atoi(argv[1]);

    return number;
}
int n_letras(int number, FILE *pf){
    
    switch(number){
        case zero:
            fINFORM(pf, "zero");
        break;
        case one:
            fINFORM(pf, "one");
        break;
        case two:
            fINFORM(pf, "two");
        break;
        case three:
            fINFORM(pf, "three");
        break;
        case four:
            fINFORM(pf, "four");
        break;
        case five:
            fINFORM(pf, "five");
        break;
        case six:
            fINFORM(pf, "six");
        break;
        case seven:
            fINFORM(pf, "seven");
        break;
        case eight:
            fINFORM(pf, "eight");
        break;
        case nine:
            fINFORM(pf, "nine");
        break;
        case ten:
            fINFORM(pf, "ten");
        break;
    }
}

int main(int argc, char *argv[]){

    int number;
    FILE *pf;

    program_name = argv[0];

    /*ENTRADA*/ number = entry(number, argc, argv);
    
    /*SALIDA*/

    if(!(pf = fopen(OUT, "wb"))){
        fINFORM(stderr, "Can't file open %s", OUT);
        return EXIT_FAILURE;
    }
    fwrite(&number, sizeof(int), N, pf);
    n_letras(number, pf);

    fclose(pf);

    return EXIT_SUCCESS;
}
