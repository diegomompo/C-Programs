#include <stdio.h>
#include <stdlib.h>

#define KWXH1 1000
#define KWXH2 1850

int main(){

    int kwxh;

    // ENTRADA

    scanf("%d", &kwxh);
    
    // CÁLCULOS

    printf(
          (kwxh < KWXH1)? "1.2\n":
          (kwxh > KWXH1 && kwxh < KWXH2)? "1\n":
           "0.9\n");
    
    // SALIDA

    return EXIT_SUCCESS;
}
