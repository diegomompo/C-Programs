#include <stdio.h>
#include <stdlib.h>
#include "decimal.h"
#include "binario.h"
#include "hexadecimal.h"

int main(){

    int opcion;
    
    printf("---------------------------------------------------------\n");
    printf("------------ 1. de Binario a Decimal     ----------------\n");
    printf("------------ 2. de Binario a Hexadecimal ----------------\n");
    printf("------------ 3. de Binario a Octal       ----------------\n");
    printf("------------ 4. de Decimal a Hexadecimal ----------------\n");
    printf("------------ 5. de Decimal a Octal       ----------------\n");
    printf("------------ 6. de Decimal a Binario     ----------------\n");
    printf("------------ 7. de Hexadecimal a Octal   ----------------\n");
    printf("------------ 8. de Hexadecimal a Binario ----------------\n");
    printf("------------ 9. de Hexadecimal a Decimal ----------------\n");
    printf("------------ 10. de Octal a Binario       ---------------\n");
    printf("------------ 11. de Octal a Decimal       ---------------\n");
    printf("------------ 12. de Octal a Hexadecimal   ---------------\n");
    printf("------------ 13. Salir                    ---------------\n");
    printf("---------------------------------------------------------\n");


    printf("Introduzca una opción: ");
    scanf("%i", &opcion);

    switch(opcion){
	
	case 1:
           Binario_Decimal();
	break;

       case 2:
   	   Binario_Hexadecimal();
	break;

	case 3:
   	   Binario_Octal();
	break;

	case 4:
	  Decimal_Hexadecimal();
	break;

	
	case 5:
   	  Decimal_Octal();
	break;

	case 6:
  	  Decimal_Binario();
	break;

	

        case 7:
   	  Hexadecimal_Octal();
	break;

	case 8 :
   	  Hexadecimal_Binario();
	break;


	case 9:
   	  Hexadecimal_Decimal();
	break;

	
	/*

	case 10:
   	  Octal_Binario();
	break;

	case 11:
   	  Octal_Decimal();
	break;

	case 12:

   	Octal_Hexadecimal();
	break;

	*/

	case 13:
	  printf("Hasta pronto\n");
	  break;

	default:
	  printf("Opción Incorrecta\n");
	  return (opcion);
	  break;
   }


    return EXIT_SUCCESS;
}
