#include <stdio.h>
#include <stdlib.h>
#include "barra.h"
#include <string.h>

#define M 3
int main(){

    char moto;
    char marca[M] = {'H', 'Y', 'S'};

    // ENTRADA

    printf("H = HONDA\n");
    printf("Y = YAMAHA\n");
    printf("S = SUZUKI\n");

    printf("Moto: ");
    scanf("%c", &moto);
    
    // CÁLCULOS

    cargando();

    if(moto == marca[0]){
        
        calculando();
        printf("Usted tiene descuento a final de año de un 5%\n");
    }
    else if(moto == marca[1]){
        
        calculando();
        printf("Usted tiene descuento a final de año de un 8%\n");
    }
    else if(moto == marca[2]){
        
        calculando();
        printf("Usted tiene descuento a final de año de un 10%\n");
    }
    else{
        
        calculando();
        printf("Usted tiene descuento a final de año de un 2%\n");
    }

    return EXIT_SUCCESS;
}
