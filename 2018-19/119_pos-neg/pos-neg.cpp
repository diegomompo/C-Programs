#include <stdio.h>
#include <stdlib.h>

#define PN 0

int main(){

    double numero;
    int resultado;

    // ENTRADA

    scanf("%lf", &numero);
    
    // CÁLCULOS
    
    printf((numero > PN) ? "positivo\n" : "negativo\n");
    
    // SALIDA

    return EXIT_SUCCESS;
}
