#include <stdio.h>
#include <stdlib.h>
#include "define_color.h"

#define ROJO 4
#define AMARILLO 2;
#define AZUL 1;
int main(){

  bool r = false, 
       y = false, 
       b = false;

  char respuesta;
  int color = 0;

  printf("¿Ves rojo?(S/N): ");
  scanf(" %c",&respuesta);
  if(respuesta == 's')
    color = ROJO;	  

  printf("¿Ves amarillo?(S/N): ");
  scanf(" %c", &respuesta);
  if(respuesta == 's')
    color = color + AMARILLO;

  printf("¿Ves azul?(S/N): ");
  scanf(" %c", &respuesta);
  if(respuesta == 's')
    color = color + AZUL;


  if(r){
    if(y){
      if(b){
        printf(WHITE "Estas viendo el color blanco");
        printf(RESET_COLOR "\n");
      }
      else{
	printf(ORANGE "Estas viendo el color naranja");
        printf(RESET_COLOR "\n");
      }
    }
    else{
      if(b){
        printf(PURPLE "Estas viendo el color morado");
        printf(RESET_COLOR "\n");
      }
      else{	
        printf(RED "Estas viendo el color rojo");
	printf(RESET_COLOR "\n");
      }
    }
  }  
  else{
    if (y){
       if (b){
         printf(GREEN "Estas viendo el color verde");
         printf(RESET_COLOR "\n");
       }
       else{
	 printf(YELLOW "Estas viendo el color amarillo");
         printf(RESET_COLOR "\n");
       }
    }
    else{
        if (b){
	 printf(BLUE "Estas viendo el color azul");
         printf(RESET_COLOR "\n");
	}
	else{
         printf(BLACK "Estas viendo el color negro\n");
	 printf(RESET_COLOR "\n");
       }
    }  
  }
  
    
      return EXIT_SUCCESS;
}

