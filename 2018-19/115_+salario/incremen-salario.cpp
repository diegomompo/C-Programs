#include <stdio.h>
#include <stdlib.h>

int main(){

    // ENTRADA
    
    double salario, total;

    scanf("%lf", &salario);

    // CÁLCULOS

    aumento = salario * 0.25;
    total = salario + aumento;
    
    // SALIDA
    
    printf("%.2lf\n", total);

    return EXIT_SUCCESS;
}
