#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "barra.h"
#include "mil.h"
#include "centenas.h"
#include "decenas.h"
#include "unidades.h"

#define D 10

int main(){

    int i = 15, numero, millar, centenas, decenas, unidades;

    // ENTRADA

    system("clear");
    printf(RED "\tNÚMEROS ROMANOS");
    printf(RESET_COLOR "\n");

    printf("Número: ");
    scanf("%d", &numero);
    
    // CÁLCULOS
    
    cargando();

    pasando();

    unidades = numero%D;
    numero = numero / D;

    decenas = numero%D;
    numero = numero / D;

    centenas = numero%D;
    numero = numero / D;

    millar = numero%D;
    numero = numero / D;

    millar2(millar);
    centenas2(centenas);
    decenas2(decenas);
    unidades2(unidades);

    return EXIT_SUCCESS;
}
