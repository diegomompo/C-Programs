#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "define_color.h"
#include "math.h"


#define DIVISOR 2
#define EXPONENTE 2

void funcion_triangulo(){  // FUNCIÓN TRIÁNGULO

    double base, altura, resultado;
    

    printf("Introduzca la base: ");
    scanf("%lf", &base);
    
    printf("Introduzca la altura: ");
    scanf("%lf", &altura);

    resultado = base * altura / DIVISOR;

     printf("\n\n"
          YELLOW "\t         " "                            "  "\n"
          YELLOW "\t         "  GREEN " Área = %6.2lf        " "\n"
          YELLOW "\t         " "                            "  RESET_COLOR "\n\n", resultado);
}
//-------------------------------------------------------------------------------

void funcion_rectangulo(){  // FUNCIÓN RECTÁNGULO

    double base, altura, resultado;

    printf("Introduzca la base: ");
    scanf("%lf", &base);
    
    printf("Introduzca la altura: ");
    scanf("%lf", &altura);

    resultado = base * altura;

     printf("\n\n"
          YELLOW "\t         " "                            "  "\n"
          YELLOW "\t         " GREEN " Área = %6.2lf        "  "\n"
          YELLOW "\t         "  "                            "  RESET_COLOR "\n\n", resultado);
}
//-------------------------------------------------------------------------------

void funcion_cuadrado(){  // CUADRADO

    double lado, resultado; 

    printf("Introduzca el lado: ");
    scanf("%lf", &lado);

    resultado = pow(lado, EXPONENTE);

     printf("\n\n"
          YELLOW "\t         " "                            "  "\n"
          YELLOW "\t         " GREEN " Área = %6.2lf        "  "\n"
          YELLOW "\t         " "                            "  RESET_COLOR "\n\n", resultado);
}
//-------------------------------------------------------------------------------

void funcion_circulo(){  // FUNCIÓN TRIÁNGULO

    double radio, resultado;

    printf("Introduzca el radio: ");
    scanf("%lf", &radio);

    resultado = M_PI * radio * radio;

    printf("\n\n"
         YELLOW "\t         " "                            "  "\n"
         YELLOW "\t         " GREEN " Área = %6.2lf        " "\n"
         YELLOW "\t         " "                            "  RESET_COLOR "\n\n", resultado);
}

