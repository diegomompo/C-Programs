#include <stdio.h>
#include <stdlib.h>

enum{
    m_uno,
    uno,
    dos,
    tres,
    cuatro,
    cinco,
    seis,
    siete,
    ocho,
    nueve
};

void unidades2(int unidades){

    switch(unidades){
    
        case m_uno:
            printf("");
            break;
        case uno:
            printf("I\n");
            break;
        case dos:
            printf("II\n");
            break;
        case tres:
            printf("III\n");
            break;
        case cuatro:
            printf("IV\n");
            break;
        case cinco:
            printf("V\n");
            break;
        case seis:
            printf("VI\n");
            break;
        case siete:
            printf("VII\n");
            break;
        case ocho:
            printf("VIII\n");
            break;
        case nueve:
            printf("IX\n");
            break;
        break;
    }
    
}

