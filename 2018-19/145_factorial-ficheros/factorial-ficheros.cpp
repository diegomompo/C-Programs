#include <stdio.h>
#include <stdlib.h>

#define fINFORMA fprintf
#define OUT "factorial.txt"

char * program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
     Usage: %s <numero>\n\n\
     Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}
int entrada(int numero, int argc, char *argv[]){

    if(argc < 2)
        print_usage (1);

    numero = atoi(argv[1]);

    return numero;
}
int factorial(int numero){

    if(numero == 0)
        return -1;

    return numero*factorial(numero-1);
}

int main(int argc, char *argv[]){

    FILE *fac;
    int numero;

    program_name = argv[0];

    /*ENTRADA*/ numero = entrada(numero, argc, argv);
    
    /*SALIDA*/

    if( !(fac = fopen(OUT, "w")))
        return EXIT_FAILURE;
    fINFORMA(fac, "La multiplicación de %i primeros números es %i\n", numero, factorial(numero));
    fclose(fac);

    return EXIT_SUCCESS;
}
