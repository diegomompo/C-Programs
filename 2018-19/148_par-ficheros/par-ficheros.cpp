#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define fINFORMA fprintf
#define OUT "par.txt"
#define D 2

char * program_name;

void print_usage(FILE *outstream, int exit_code){

    fINFORMA(outstream, "\
            Calculate if a number is par or not - \n\n\
            Usage:\n\
            -h <help> \t h: Help\n\n\
            -r <run> \t r: run the program\n\n\
            %s -r <numero>\n\n\
            Number: Positive Integer\n\n", program_name, program_name);
    exit(exit_code);
}
int entrada(int numero, int argc, char *argv[], FILE *res){

    if(argc < 1)
        return EXIT_FAILURE;

    numero = atoi(argv[2]);

    return numero;
}
int comprobar(int numero, FILE *res){

    if(numero%D == 0){
        fINFORMA(res, "El número %i es par\n", numero);
    } else{

        fINFORMA(res, "El número %i no es par\n", numero); 
    }

}

int main(int argc, char *argv[]){

    FILE *res;
    int numero;
    int c;
    program_name = argv[0];


    /*CÁLCULOS*/

    while((c = getopt(argc, argv, "hir:")) != -1){

        switch(c){

            case 'h':
                print_usage(stdout, 0);
                break;
            case 'r':
                numero = entrada(numero, argc, argv, res);
                comprobar(numero, res);
                break;
            case '?':
                if(optopt != 'h' && optopt != 'i' && optopt != 'r')
                    fINFORMA(stderr, "Invalid option\n");
                else
                    fINFORMA(stderr, "Option argument missing\n");

                print_usage(stderr, 1);
                break;
            default:
                abort();
                break;
        }
    }
    fclose(res);

    /*SALIDA*/

    return EXIT_SUCCESS;
}
