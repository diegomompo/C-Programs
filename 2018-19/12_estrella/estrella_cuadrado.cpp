#include <stdio.h>
#include <stdlib.h>

int main(){

 int l = 5;

 for(int f=0; f<l; f++){
    for(int c=0; c<l; c++)
      if(c == 0 || c == l-1 || f == 0 || f == l-1)
        printf("*");
      else
        printf(" ");  
    printf("\n");
    }

    return EXIT_SUCCESS;
}


 //-------------------------------------------------------------------------------------------------------
 
 // SOLUCIÓN 2

/*    
  int c,f;
  int l = 10;

  for (c=0;c<l;c++){
    printf("*");
  }

  printf("\n");

  for (f=0;f<l-2;f++){
    printf("*");
    for(c=0;c<l-2;c++){
      printf(" ");
    }
    printf("*\n");
  }

  for (c=0;c<l;c++){
    printf("*");
  }
*/

 //-------------------------------------------------------------------------------------------------------------
 
 // SOLUCIÓN 3

/*    
    int l = 5;
    int f = 0;

    for(f=0; f<=l; f++){
        if((f == 0) || (f == l))
            for(int c=0; c<=l; c++){
              printf("*");
            }
        else{
        
            printf("*");
            for(int c=0; c<=l-2;c++)
            printf(" ");
            printf("*");   
        }  
    printf("\n");
    }
        
    
*/



