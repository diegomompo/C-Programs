#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define N 0x100

int entry(int number, int argc, char *argv[]){

    if(argc < 2)
        return EXIT_FAILURE;

    number = atoi(argv[1]);

    return number;
}

int main(int argc, char *argv[]){

    int number;
    int number2 = 5;
    char **word = NULL;
    char buffer[N];
    int j;
    int len;

    /*ENTRY*/ number = entry(number, argc, argv);
    
    srand(time(NULL));

    /*CALCULATED*/

    for(int i=0; i < number2; i++){
      word = (char**) realloc(word, (i+1) * sizeof(char*));
      j = rand() % number2;

      printf("Name %i: ", i);
      fgets(buffer, N, stdin);

      len = strlen(buffer);
      word[j] = (char*) malloc(len);
      strncpy(word[j], buffer, len);  
    }

    for(int k=0; k<number2; k++)
        printf("%s\n", word[k]);

    free(word);
    /*SALIDA*/

    return EXIT_SUCCESS;
}
