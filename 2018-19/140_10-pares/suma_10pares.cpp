#include <stdio.h>
#include <stdlib.h>
#include "barra.h"
#define INFORMA printf
#define P 10
#define M 2

char * program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
     Usage: %s <numero>\n\n\
     Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}
int entrada(int numero, int argc, char *argv[]){

    if(argc < 2)
        print_usage (1);
    
    numero = atoi(argv[1]);

    cargando();

    return numero;

}
int suma(int numero, int res){

    res = 0;
    int diez = M*P;

    calculando();

    for(int i=numero; i<(numero+diez); i++)
        if(i%M==0 && i>=2){
            res += i;
        }

    return res;
}

int main(int argc, char *argv[]){
    int numero;
    int res;

    program_name = argv[0];

    /*ENTRADA*/ numero = entrada(numero, argc, argv);
    
    /*CÁLCULOS*/ res = suma(numero, res);
    
    /*SALIDA*/ INFORMA("La suma de los 10 primeros numeros pares es %i\n", res);

    return EXIT_SUCCESS;
}
