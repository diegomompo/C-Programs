
#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>


enum Opciones { // enumeración de la opciones

	cargar_datos,
	escribir_datos,
	insertar_datos,
	escribir_tabla,
	salir,
	OPCIONES

};

struct TAlumno{

	char Nombre[40];
	int edad;

}TablaAlumno;

const char *texto[] = { // declaración de las opciones

	"Cargar los datos",
	"Escribir un dato",
	"Insertar un dato",
	"Escribir en la tabla",
	"Salir",

};

enum Opciones menu(int op){ // declaración del menú

	printf("\t\t OPCIONES PARA TABLA\n");
	printf("\t\t========================================\n");
	for(int i=00; i<OPCIONES; i++)
		printf("\t\t%i. %s\n", i, texto[i]);

	printf("\t\t Opción a realizar: ");
	scanf("%d", &op);
	__fpurge(stdin);
	
	return (enum Opciones) op;	

}

void CargarDatos (FILE *pf, struct TAlumno TablaAlumno[10]){

	struct TAlumno;
	char buffer[100];

        fscanf(pf, "%s", buffer);
	printf("%s", buffer);

}

void EscribirDatos (FILE *pf, struct TAlumno TablaAlumno[10]){

	struct TAlumno;
	int i = 0;

	while( (i = getc(pf)) !=EOF){
		printf("Dime tu nombre: ");
		scanf("%c", TablaAlumno->Nombre);

		printf("Introduce la edad:");
		scanf("%i", &TablaAlumno->edad);

		i++;
	}

	fwrite(TablaAlumno, sizeof(TablaAlumno), 1, pf);
	
}

void InsertarDatos (FILE *pf, TAlumno TablaAlumno[10]){

        struct TAlumno;
	char buffer[100];

	fprintf(pf, buffer);
	fprintf(pf, "%s", "\nDiego.");
	
}

int main(int argc, char *argv[]){
	
	int op;
	struct TAlumno* TablaAlumno;
	int opcion;
	FILE *pf;

	volver:

	printf("\n");

	opcion = menu(op);

	if(!(pf= fopen("algo.txt", "rt"))){
		fprintf(stderr, "No se ha podido leer el fichero\n");
		return 0;
	}

	switch(opcion){
	
	   case 0: 
	   CargarDatos(pf, TablaAlumno);
	   break;
	   
	   case 1: 
           EscribirDatos(pf, TablaAlumno);
           break;

	   case 2: 
           InsertarDatos(pf, TablaAlumno);
           break;
	   
 	   case 3: 
           return EXIT_SUCCESS;
           break;

	   case 4: 
           printf("Adiós\n");
	   return EXIT_SUCCESS;
           break;

	  default:
	   printf("Opción incorrecta. Prueba otra vez");
	   goto volver;
	   break;
	}

	fclose(pf);

	return EXIT_SUCCESS;
}





/*

Gestionar datos alumno

Nombre de 40 caracteres

edad

10 alumnos

1. Cargar datos

2. Escribir datos

3. insertar en la tabla

4. Escribir la tabla en la pantalla

5. borrar un dato de la tabla

6. Salir.
*/
