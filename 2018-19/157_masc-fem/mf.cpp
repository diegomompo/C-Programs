#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int entry(int number, int argc, char *argv[]){

  if(argc < 1)
      return EXIT_FAILURE;

  number = atoi(argv[0]);

  return number;

}

int main(int argc, char *argv[]){

    int number;
    char *person;
    int p = 1;
    int male;
    int female;
    int other;
    int len;


    /*ENTRY*/ number = entry(number, argc, argv);
    
    /*CALCULATED*/

    for(int i=0; i<number; i++){
      person = (char*) realloc(person, p * sizeof(char*));
      printf("Gender of Person %i: ", i);
      person = getchar();

      if(person == 'm')
          male++;
      else if(person == 'f')
          female++;
      else
          other++;

      p++;

    }
    
    /*RETURN*/

    printf("There are %i males, %i females and %i other", male, female, other);

    free(person);
    return EXIT_SUCCESS;
}
