#include <stdio.h>
#include <stdlib.h>

char * program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
     Usage: %s <numero>\n\n\
     Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}
int entrada(int numero, int argc, char *argv[]){

    if(argc < 2)
        print_usage (1);

    numero = atoi(argv[1]);

    return numero;
}
int primo(int numero, int x){
  if(x==numero)
      return 1;
  else
    if(numero%x==0)
      return 1+primo(numero, x+1);
    else
      return 0+primo(numero, x+1);
}

int main(int argc, char *argv[]){

    int numero, x=1, d;
    program_name = argv[0];

    /*ENTRADA*/ numero = entrada(numero, argc, argv);
    
    /*CÁLCULOS*/ d = primo(numero,x); 
    
    /*SALIDA*/

    if(d==2)
        printf("%i es primo\n", numero);
    else
        printf("%i no es primo\n", numero);

    return EXIT_SUCCESS;
}
