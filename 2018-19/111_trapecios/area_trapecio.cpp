#include <stdio.h>
#include <stdlib.h>

#define BAS 2

int main(){

    double base[BAS];
    double altura;
    double area;

    // ENTRADA

    for(int i = 0; i < BAS; i++)
        scanf("%lf", &base[i]);

    scanf("%lf", &altura);
    
    // CÁLCULOS
    
    if(base[0] > base[1]){
    
      area = ((base[0] - base[1])*altura)/2; 
    }
    else{
    
      area = ((base[1] - base[0])*altura)/2;   
    }
    
    // SALIDA

    printf("%.2lf\n", area);

    return EXIT_SUCCESS;
}
