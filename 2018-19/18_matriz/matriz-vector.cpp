#include <stdio.h>
#include <stdlib.h>

#define DIMA 3
#define DIMB 3

int main(){

    double a0[DIMA], a1[DIMA], b[DIMB], resultado = 0, resultado2 = 0;

    // ENTRADA
    
        for(int i=0; i<DIMA; i++){
          printf("A0%i\n", i);
          scanf("%lf", &a0[i]);

          printf("A1%i\n", i);
          scanf("%lf", &a1[i]);

          printf("B%i0\n", i);
          scanf("%lf", &b[i]);

          resultado += a0[i] * b[i];
          resultado2 += a1[i] * b[i];

        }

    // SALIDA
        
        printf("%.2lf\n", resultado);
        printf("%.2lf\n", resultado2);

    return EXIT_SUCCESS;
}
