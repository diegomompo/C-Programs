#include <stdio.h>
#include <stdlib.h>

enum {

  nulo,
  enero,
  febrero,
  marzo,
  abril,
  mayo,
  junio,
  julio,
  agosto,
  septiembre,
  octubre,
  noviembre,
  diciembre 
};
int main(){

    int mes;

    // ENTRADA
  
    printf("Mes(número): ");
    scanf("%i", &mes);
  
    // CÁLCULOS

    switch(mes){
    
        case nulo:
            printf("Él número introducido no corresponde al mes especificado\n");
            break;
        case enero:
            printf("ENERO\n");
            break;
        case febrero:
            printf("FEBRERO\n");
            break;
        case marzo:
            printf("MARZO\n");
            break;
        case abril:
            printf("ABRIL\n");
            break;
        case mayo:
            printf("MAYO\n");
            break;
        case junio:
            printf("JUNIO\n");
            break;
        case julio:
            printf("JULIO\n");
            break;
        case agosto:
            printf("AGOSTO\n");
            break;
        case septiembre:
            printf("SEPTIEMBRE\n");
            break;
        case octubre:
            printf("OCTUBRE\n");
            break;
        case noviembre:
            printf("NOVIEMBRE\n");
            break;
        case diciembre:
            printf("DICIEMBRE\n");
            break;
  }


    return EXIT_SUCCESS;
}
