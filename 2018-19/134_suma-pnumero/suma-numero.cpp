#include <stdio.h>
#include <stdlib.h>

#define INFORMA printf
char *program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
    Usage: %s <numero>\n\n\
    Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}

int entrada(int numero, int argc, char *argv[]){

    if(argc < 2)
        print_usage (1);
     numero = atoi (argv[1]);
   
}
int suma_num(int numero, int suma){

    for(int i=0; i<=numero; i++)
        suma += i;

    return suma;
}

int main(int argc, char *argv[]){

    int numero; 
    int suma;
    program_name = argv[0];

    /*ENTRADA*/ numero = entrada(numero, argc, argv);
    
    /*CÁLCULOS*/ suma = suma_num(numero, suma);
    
    /*SALIDA*/ INFORMA("La suma de los %i primeros numeros es %i\n", numero, suma);

    return EXIT_SUCCESS;
}
