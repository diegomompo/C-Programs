#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "define_color.h"
#include "funciones.h"
#include "strings.h"
#define V 100
#define DELAY 50000

void pila_int(){

    struct variable;

    for(int veces=0; veces<=V; veces++){
              for(int columnas=0; columnas < veces; columnas++)
                  fprintf(stderr, GREEN_CLARO "=");
              fprintf(stderr, GREEN_CLARO "Pila introducida. Por favor, espere %i%%\r", veces);
              usleep(DELAY);
            }
    printf(RESET_COLOR);
}

void calculando(){

    system("clear");

    struct variable;
    for(int veces = 0; veces<=V; veces++){
              for(int columnas = 0; columnas < veces; columnas++)
                  fprintf(stderr, GREEN_CLARO "=");
              fprintf(stderr, GREEN_CLARO "Calculando la operación. Por favor, espere %i%%\r", veces);
              usleep(DELAY);
            }
    printf(RESET_COLOR);
    printf("\n");
}
void borrando(){

    struct variable;
    for(int veces = 0; veces<=V; veces++){
              for(int columnas = 0; columnas < veces; columnas++)
                  fprintf(stderr, GREEN_CLARO "=");
              fprintf(stderr, GREEN_CLARO "Borrando el número. Por favor, espere %i%%\r", veces);
              usleep(DELAY);
            }
    printf(RESET_COLOR);
}

