#include <stdio.h>
#include <stdlib.h>

#define S 10.5

int main(){

    double nota;
    char resultado;
    // ENTRADA

    scanf("%lf", &nota);
    
    // CÁLCULOS
    
    printf((nota > S) ? "Aprobado\n" :  "Suspenso\n");
    

    return EXIT_SUCCESS;
}
