#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <ncurses.h>
#include "operandos.h"
#define N 10
#define R 9
#define V 100
#define DELAY 50000

struct TPila {
      double sav_num[N];
      int cima;
      int cabeza;
};   

enum Operaciones {
    
    suma,
    resta,
    multiplicacion,
    division,
    OPERACIONES
};

const char *texto[] = {

    "sumar",
    "resta",
    "multiplicar",
    "dividir",
};

enum Operaciones sav_op[R];

/*double pop(struct TPila *p ){

    int sav_num;
    sav_num = p->sav_num[p->cima];

    return sav_num;
}*/

struct TGeneral{
      double numero;
      char elegir;
      int i = N-1;
      int j;
      enum Operaciones op;

}; 

#include "menu.h"
#include "funciones.h"

int main(int argc, char *argv[]){

    struct TPila pila;
    struct TGeneral variables;

    double (*operaciones[])(double , double){
        &sum, &res, &mul, &divi};
    double (*in)(double, TPila*){ &intro };

    bzero(&pila, sizeof(pila));

    /* ENTRADA */

    (*in)(variables.numero, &pila);
    pila_int();

    /*CÁLCULOS*/

    do{
        variables.i--;
        variables.j = 1;

        system("clear");

        for (int i=N-1; i>=0; i--)
            printf("%lf\n", pila.sav_num[i]);

        variables.op =  menu();

        calculando();

        pila.sav_num[N-1] = (*operaciones[variables.op])(pila.sav_num[variables.i], pila.sav_num[N-1]);

        borrando();

        bzero(&pila.sav_num[variables.i], sizeof(pila.sav_num[variables.i]));
        
    }while(pila.sav_num[0] != '\0');

    /*SALIDA*/

    printf("\n");

    printf("EL resultado es %lf\n", pila.sav_num[R]);

    return EXIT_SUCCESS;
}
