#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 100

int main(int argc, char *argv[]){

  char a[N];
  char *p;

  fgets(a, N, stdin);

  p = (char *) malloc(strlen(a) + 1);

  strncpy(p, a, N);  

  printf("s=%s\n", p);

  printf("strlen(a) = %d\n", strlen(a) + 1);

    return EXIT_SUCCESS;
}

