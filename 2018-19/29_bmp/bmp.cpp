#include <stdio.h>
#include <stdlib.h>

#define imagen "panda.bmp"
#define WIDTH 1192L
#define HEIGHT 984L
#define Bpp 3
#define ROW_SIZE (WIDTH * Bpp + 3)/4 *4
#define IMAGE_OFFSET 0X8A

unsigned short width, height;
unsigned offset;

// const char *five[] = { "█", "▓", "▒", "░", " " };
const char *five[] = { "█", " " };

/*const char * five_colors (
        unsigned char r,
        unsigned char g,
        unsigned char b) {

    int media = (r + g + b ) / 3;
    return five[media / 51];
}*/
const char * two_colors (
      unsigned char r,
      unsigned char g,
      unsigned char b,
      int valim){
  int media = (r + g + b ) / 3;
  return media > valim ? "█" : " ";
  
  }


int main(){
    unsigned char *image;
    image = (unsigned char *) malloc(HEIGHT * ROW_SIZE);
    FILE *pf;
    // abrir un fichero
    // Avanzar a la posicion 8a
    // leer 619 x 349 x 3
    // cerrar el fichero
 
  if(!(pf = fopen(imagen, "r"))){
    fprintf(stderr, "Mac donde estas?\nRai no te veo.\n");
    return EXIT_FAILURE;
    }

    fseek(pf, 0xa, SEEK_SET);
    fread(&offset, 4, 1, pf);
    
    fseek(pf, 0x12, SEEK_SET);
    fread(&width, 2, 1, pf);

    fseek(pf, 0x14, SEEK_SET);
    fread(&height, 2, 1, pf);

    fseek(pf, IMAGE_OFFSET, SEEK_SET);
    fread(image, 1, HEIGHT * ROW_SIZE, pf);
    
    fclose(pf);

    printf("%lu, %lu\n", sizeof(short), sizeof(int));

    free(image);
  return EXIT_SUCCESS;
}
