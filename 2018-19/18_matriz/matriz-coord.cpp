#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIMA 1
#define DIMB 3

int main(){

    double a0[DIMA], a1[DIMA], coseno, seno, angulo, resultado = 0, resultado2 = 0;

    // ENTRADA
    
        for(int i=0; i<DIMA; i++){
          printf("A0%i\n", i);
          scanf("%lf", &a0[i]);

          printf("A1%i\n", i);
          scanf("%lf", &a1[i]);

          printf("angulo\n");
          scanf("%lf", &angulo);

          // CALCULOS

         coseno = cos(angulo*M_PI/180);
         seno = sin(angulo*M_PI/180);
         
         resultado += (a0[i] * coseno) + (a1[i] * -seno);
         resultado2 += (a0[i] * seno) + (a1[i] * coseno);
          
        }

    // SALIDA
   
        printf("%.2lf\n", resultado);
        printf("%.2lf\n", resultado2);

    return EXIT_SUCCESS;
}

