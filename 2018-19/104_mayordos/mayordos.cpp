#include <stdio.h>
#include <stdlib.h>

int main(){

    int numero1;
    int numero2;

    scanf("%d", &numero1);
    printf("\n");
    scanf("%d", &numero2);
    printf("\n");

    printf("%d\n", (numero1 > numero2)? numero1: numero2);

    return EXIT_SUCCESS;
}

// OTRAS SOLUCIONES

/* Solución 2

   int may;
   
   if(x > y){

   may = x;

   }
   else{
    
     may = y;
   }

   printf("%d\n" , may );
   return 0;
*/


/* Solución 3

   int may;
   
   if(numero1 > numero2){
       printf("%d\n", numero1);
   }
   else{
      printf("%d\n", numero2);    
   }

   return 0;
*/
