#include <stdio.h>
#include <stdlib.h>

#define N 2

int main(){

    int numero[N];

    // ENTRADA

    for(int i=0; i<N; i++)
    scanf("%d", &numero[i]);
    
    // CÁLCULOS

    if(numero[0] > numero[1]){
    
      printf("%d es mayor que %d\n", numero[0], numero[1]);
    }
    else{
    
      printf("%d es menor que %d\n", numero[0], numero[1]);
 
    }
    
    // SALIDA

    return EXIT_SUCCESS;
}
