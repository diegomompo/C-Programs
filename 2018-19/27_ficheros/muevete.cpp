#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define FILENAME "nombre.txt"

int main(int argc, char *argv[]){

    FILE *pf;
    int pos;
    
    /*SALIDA*/

    srand(time (NULL));

    if(!(pf = fopen(FILENAME, "r"))){
        fprintf(stderr, "Aín.\n");
        return EXIT_SUCCESS;
    }
    pos = rand() % 100;

    fseek (pf, pos, SEEK_SET);

    printf("Pos: %i => %c\n", pos, (char) getc(pf));

    fclose(pf);

    return EXIT_SUCCESS;
}
