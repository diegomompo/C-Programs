#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX 3
#define D 2
#define N 0x10
#define OUT "average.txt"
#define fINFORM fprintf

char * program_name;

double res[N];

void push(double number){
    static int i = 0;
    res[i++] = number;
}

void print_usage(FILE *outstream, int exit_code){

    fprintf( outstream, 
    "This is a program to calculate arithmetic average with two numbers-\n\n"
    "Usage:\n"
    "-h <help> \t Print help of the program\n\n"
    "-i <number> \t Introduced number in the program \n\n" 
    "-c <number> \t Calculated the artithmetic average of the program\n\n"
    "-r <number> \t Print the result of the program\n\n"

    "Example: %s -i <number> <number> -cr\n\n\n"
     
    "Number: Positive\n", program_name, program_name);

    exit(exit_code);
}

double entry(double number, int argc, char *argv[], FILE *me){
    
    if(argc < 0)
        return EXIT_FAILURE;

    for(int i = 2; i<=MAX; i++){
        number = atof(argv[i]);
        push(number);
    }
}
double calculated(double average, FILE *me){
    
    average = res[0] + res[1];

    average /= D;

    return average;
}

int main(int argc, char *argv[]){

    FILE * me;
    double number;
    double average;
    int c;

    
    program_name = argv[0];

    if(!(me = fopen(OUT, "w")))
        return EXIT_FAILURE;

    while((c = getopt (argc, argv, "h:i:c:r")) != -1){
    
        switch(c){

            case 'h':
                print_usage(stdout, 0);
                break;
            case 'i':
                entry(number, argc, argv, me);
            case 'c':
                average = calculated(average, me);
            case 'r':
                fINFORM(me, "The average of %lf and %lf is %.2lf\n", res[0], res[1], average);
                break;
            case '?':
                if(optopt != 'i' && optopt != 'c' && optopt != 'r')
                    fINFORM(stderr, "Invalid option\n");
                else
                    fINFORM(stderr, "Option argument missing\n");

                print_usage(stderr, 1);
                break;
            default:
                abort();
                break;
        }
    }
    fclose(me);

    return EXIT_SUCCESS;
}
