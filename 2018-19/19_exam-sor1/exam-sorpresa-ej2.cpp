#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Función punto de entrada */
int  main(){
    int potencia = 5;
    int resultado = 1;
    int x = 2;

    /* Opcion 1 */
    for (int vez=0; vez<potencia; vez++)
        resultado *= x;

    /* Alternativa */
    pow(x,potencia);

    return EXIT_SUCCESS;
}

