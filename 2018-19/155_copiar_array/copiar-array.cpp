#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define fINFORM fprintf
#define OUT "array.dat"
char * program_name;

void print_usage(FILE *outstream, int exit_code){

    fINFORM(outstream,
    "This a program that reserved a dinamic memory for print a copy array for other array . - \n\n"
    "Usage:"
    "-h <help> \t h: Help the program\n\n"
    "-i <introduce>  \t i: introduce the numbers\n"
    "-c <number> \t c: Copy the array for other array"
    "Example: %s -i <number> -c\n\n"
    "Number: Positive Integer >= 2\n\n", program_name);
    exit(exit_code);
}
int entry(int number, int argc, char *argv[], FILE *pf){

    if(argc < 2)
        return EXIT_FAILURE;

    number = atoi(argv[2]);

    return number;
}
void entry2(int number, int *guardar, FILE *pf){

    for(int i=0; i<number; i++)
      scanf("%i", &guardar[i]);

}
void calculated(int number, int *guardar, FILE *pf){

    int cop[number];

    for(int i=0; i<number; i++){
        cop[i] = guardar[i];
        fwrite(&cop[i], sizeof(int), 1, pf);
    }
}
int main(int argc, char *argv[]){

    int number;
    int *guardar;
    int c;
    int cop[number];

    FILE *pf;

    program_name = argv[0];

    if(!(pf = fopen(OUT, "wb"))){
        fINFORM(stderr, "Can't open file %s", OUT);
        return EXIT_FAILURE;
    }
    
    guardar = (int *) malloc(number);

    while((c = getopt(argc, argv, "h:i:c")) !=-1){
   
      switch(c){
          case 'h':
            print_usage(stdout, 0);
            break; 
          case 'i':
            /* ENTRY */ number = entry(number, argc, argv, pf); 
            /* ENTRY */ entry2(number, guardar, pf);
            break;
          case 'c':
            /*CALCULATED*/ calculated(number, guardar, pf);
            break;
          case '?':
            if(optopt != 'i' && optopt != 'c')
              fINFORM(stderr, "Invalid Option\n");
            else
              fINFORM(stderr, "Option argumnet missing\n");

             print_usage(stderr, 1);
             break;
          default:
             abort();
             break;
        }
     }
  
    fINFORM(pf, "\n");

    free(guardar);
    fclose(pf);
    return EXIT_SUCCESS;
}
