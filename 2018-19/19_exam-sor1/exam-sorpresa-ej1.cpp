#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define M 2
#define K 4
#define N 3

int main(){

    int fila, columna;

    double a[M][K] = { 
                         {2,3,5,1}, 
                         {3,1,4,2}
                       },
           b[K][N] = { 
                         {5,2,1}, 
                         {3,-7,2}, 
                         {-4,5,1}, 
                         {2,3,-9}
                       },
           c[M][N];

    // ENTRADA

    printf("Fila: ");
    scanf("%d", &fila);
    printf("Columna: ");
    scanf("%d", &columna);
    
    // CÁLCULOS

    c[fila][columna] = 0;

        for(int k = 0; k<K; k++)
          c[fila][columna] += a[fila][k] * b[k][columna];

      /* SALIDA */ printf("C[%d][%d] = %.2lf\n", fila, columna, c[fila][columna]);


    return EXIT_SUCCESS;
}

