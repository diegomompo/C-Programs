#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 1
#define MEDIO 500000
#define UNO 1000000
#define UNO_MEDIO 1500000
#define DOS_MEDIO 2500000


int Parte1_ENG(){

    for(int i=0; i<VECES; i++){

    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(UNO_MEDIO);
    fputc('\a', stderr);
    usleep(MEDIO);
    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(UNO_MEDIO);
    fputc('\a', stderr);
    usleep(MEDIO);
    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(MEDIO);
    fputc('\a', stderr);
    usleep(MEDIO);
    fputc('\a', stderr);
    usleep(MEDIO);
    fputc('\a', stderr);
    usleep(MEDIO);
    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(UNO);
    fputc('\a', stderr);
    usleep(UNO);

    }
    return EXIT_SUCCESS;
}
