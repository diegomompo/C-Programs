#include <stdio.h>
#include <stdlib.h>
#include "barra.h"

#define INFORMA printf
#define P 2

char * program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
    Usage: %s <numero>\n\n\
    Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}

int entrada(int numero, int argc, char *argv[]){

    if(argc < 2){
    
        print_usage (1);
    }
    numero = atoi (argv[1]);

    cargando();

    return numero;  
}

int suma(int numero, int suma_pares, int suma_impares, int total, int i, int neg){

    calculando();

    i=1;

    while(i<=numero){

      if(i%P == 0 && i>=P){
      
          neg = i * (-1);
          suma_pares += neg;
      }else{
      
          suma_impares += i;
      }
      i++;
    }

    total = suma_pares + suma_impares;

    return total;
}

int main(int argc, char *argv[]){

    int numero, suma_pares=0, suma_impares=0, total, i, neg;
    program_name = argv[0];

     /*ENTRADA*/ numero = entrada(numero, argc, argv);
    
    /*CÁLCULOS*/ total = suma(numero, suma_pares, suma_impares, total, i, neg);
    
    /*SALIDA*/ INFORMA("La suma total es %i\n", total);

    return EXIT_SUCCESS;
}
