#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


#define R 300

#define WIDTH 1920
#define HEIGHT 1080

/* No cambiable */
#define XC WIDTH / 2
#define YC HEIGHT / 2
#define SIZE WIDTH * HEIGHT
#define BPP 4
#define TOTALBYTES SIZE * BPP
/* Fin No Cambiable */

/* Función punto de entrada */
int  main(){

    unsigned *video;
    int fd = open("/dev/fb0", O_RDWR);
    if (fd < 0 ){
        fprintf (stderr, "Unable to open framebuffer.\n");
        exit (1);
    }
    video = (unsigned *) mmap(NULL, TOTALBYTES,
            PROT_READ | PROT_WRITE, MAP_SHARED,
            fd, 0);

    if (video <= (unsigned *) 0){
        fprintf (stderr, "Unable to map framebuffer.\n");
        exit (1);
    }

    int x, y;
    for (double angulo=0; angulo<2*M_PI; angulo+=.005){
        x = XC + R * cos (angulo);
        y = YC - R * sin (angulo);
        *(video + y * WIDTH + x) = 0xFFFFFFFF;
    }

    close(fd);
    munmap(video, TOTALBYTES);

    return EXIT_SUCCESS;
}
