#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "define_color.h"
#include "math.h"


#define N     100
#define DELAY 50000


int cargando(){

     struct variable;
   
  for (int veces=0; veces<N; veces++){
    for (int columna=0; columna<veces; columna++)
      fprintf(stderr, YELLOW "=");
    fprintf(stderr, YELLOW "cargando numero. Por favor, espere.%i%%\r",veces);
    usleep(DELAY);
  }
  printf(RESET_COLOR "\n");
}

// -----------------------------------------------------------------------------
int pasando(){

      struct variable;

  for (int veces=0; veces<N; veces++){
    for (int columna=0; columna<veces; columna++)
      fprintf(stderr, YELLOW "=");
    fprintf(stderr, YELLOW "Pasando numero a numero romano. Por favor, espere %i%%\r",veces);
    usleep(DELAY);
  }
  printf(RESET_COLOR "\n");
}

//---------------------------------------------------------------------------------

int imprimir(){

      struct variable;

  for (int veces=0; veces<N; veces++){
    for (int columna=0; columna<veces; columna++)
      fprintf(stderr, YELLOW "=");
    fprintf(stderr, YELLOW "Imprimiendo número. Por favor, espere %i%%\r",veces);
    usleep(DELAY);
  }
  printf(RESET_COLOR "\n");
}
