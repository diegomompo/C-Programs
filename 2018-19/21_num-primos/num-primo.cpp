#include <stdio.h>
#include <stdlib.h>

char *program_name;

void print_usage(int exit_code){

    FILE *f = stdout;
    if(exit_code !=0)
        f = stderr;
    fprintf(f,
    "Checks for primality of the argument. - \n\n\
    Usage: %s <numero>\n\n\
    Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}

int entrada(int numero, int argc, char *argv[]){

    if(argc < 2)
        print_usage (1);
     numero = atoi (argv[1]);
   
}

bool es_primo (int numero) {

    bool primo = true;

     for(int i=numero/2; primo && i>1; i--){
        if(numero%i == 0)
          primo = false;
    }
    return primo;
}

int main(int argc, char *argv[]){
   
    int numero; 
    program_name = argv[0];

    /*ENTRADA*/ numero = entrada(numero, argc, argv);

    /*CALCULO*/ es_primo(numero);

    /*SALIDA*/ printf("%s es primo el %i\n", es_primo(numero)? "Si" : "No", numero);

    return EXIT_SUCCESS;
}

