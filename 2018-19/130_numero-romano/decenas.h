#include <stdio.h>
#include <stdlib.h>

enum decenas {
    m_diez,
    diez,
    veinte,
    treinta,
    cuarenta,
    cincuenta,
    sesenta,
    setenta,
    ochenta,
    noventa
};

void decenas2(int decenas){

    
    switch(decenas){
    
        case m_diez:
            printf("");
            break;
        case diez:
            printf("X");
            break;
        case veinte:
            printf("XX");
            break;
        case treinta:
            printf("XXX");
            break;
        case cuarenta:
            printf("XL");
            break;
        case cincuenta:
            printf("L");
            break;
        case sesenta:
            printf("LX");
            break;
        case setenta:
            printf("LXX");
            break;
        case ochenta:
            printf("LXXX");
            break;
        case noventa:
            printf("XC");
            break;
        break;
    }
}

