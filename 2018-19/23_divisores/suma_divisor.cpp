#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX 0x100
#define INFORMA(...) if(verbose_flag) printf(__VA_ARGS__);

bool help_flag = false,
     verbose_flag = false;
const char *program_name; 
  
void print_usage(FILE *pf, int exit_code){
    fprintf(pf, "%s lo que sea.\n", program_name);
    exit(exit_code);
}

int entrada(int n, int argc, char *argv[]){

    INFORMA("Voy a coger el parámetro de terminal\n")

    n = atoi (argv[optind]);

    INFORMA("He recogido el parámetro %i de la terminal\n", n)
    
    return n;
}

int  divisor(int n, int lista[MAX]){
    int pos = 0;

    for(int d=1; d<n; d++){
        if(n % d == 0)
            lista[pos++] = d;
   }
    return pos;
}
 void imprime (int lista[MAX], int n){

    for(int i=0; i<n; i++){
      printf("%i \n", lista[i]);
    }
    printf("\n");
}

int suma(int n, int total){

    total = 0;
    for(int i=1; i<n;i++){
        if(n%i==0)
            total += i;
    }
    return total;
}


int main(int argc, char *argv[]){

    int n;
    int lista[MAX];
    int cuantos;
    int total;
    int o;

    while( (o = getopt(argc, argv, "hv")) != -1){
      switch(o){
        case 'h':
            help_flag = true;
            break;
        case 'v':
            verbose_flag = true;
            break;
        case '?':
            print_usage (stderr, 1);
            break;
      }

    program_name = argv[0];

    }
    if(argc < 2)
        return EXIT_FAILURE;

    if(help_flag)
        print_usage (stdout, 0);

    /*ENTRADA*/ n = entrada(n, argc, argv);
    
    /*CÁLCULOS*/ 
    cuantos = divisor(n, lista);
    
    /*SALIDA*/ 
    
    INFORMA("He encontrado %i divisores\n", cuantos)

    imprime(lista, cuantos);
    total = suma(n, total);
    
    printf("%i\n", total);

    if(total == n){

        printf("El número es perfecto\n");
    }else{
   
        printf("El número no es perfecto\n"); 
    }


    return EXIT_SUCCESS;
}
