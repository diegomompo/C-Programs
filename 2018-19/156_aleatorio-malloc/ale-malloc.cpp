#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define N 1000
#define P 2
#define INFORM printf
#define fINFORM fprintf
#define OUT "random.dat"

char * program_name;

void print_usage(FILE *outstream, int exit_code){

    fprintf(outstream,
    "Porgram that the user introduce a quantity randoms numbers between 1 and 1000 and the program generate the numbers randoms. - \n\n"
    "Usage:"
    "-h <help> \t h: Help the program\n\n"
    "-i <introduce>  \t i: introduce a number\n"
    "-c <number> \t c: generate numbers randoms and group by odd and even"
    "Example: %s -i <number> -c\n\n"

    "Number: Positive Integer\n\n", program_name);
    exit(exit_code);
}
int entry(int number, int argc, char *argv[]){

    if(argc < 1)
        return EXIT_FAILURE;

    number = atoi(argv[2]);
}
void calculated(int number, int *save, FILE *pf){

    srand (time(NULL)); 

    for(int i = 0; i<number; i++)
    save[i] = rand() % N; 

    for(int i = 0; i<number; i++){
      if(save[i] % P == 0)
        fwrite(&save[i], sizeof(int), 1, pf);
    }
    printf("\n");

    for(int i = 0; i<number; i++){
      if(save[i] % P != 0)
        fwrite(&save[i], sizeof(int), 1, pf);
    }
    
    printf("\n");


}

int main(int argc, char *argv[]){

    int number;
    int *save = (int *) malloc(number);
    FILE *pf;
    FILE *pf2;
    int c;

    program_name = argv[0];
    

    if(!(pf = fopen(OUT, "wb"))){
       fINFORM(stderr, "Can't open file %s", OUT);
       return EXIT_FAILURE;
    }

    while((c = getopt(argc, argv, "h:i:c")) != -1){
      
        switch(c){
          case 'h':
            print_usage(stdout, 0);
            break;
          case 'i':
            /*ENTRY*/ number = entry(number, argc, argv);
            break;
          case 'c':
            /*CALCULATED*/ calculated(number, save, pf);
            break;
          case '?':
            if(optopt != 'i' & optopt != 'c')
                fINFORM(stderr, "Invalid Option\n");
            else
                fINFORM(stderr, "Option argument missing\n");
           
            print_usage(stderr, 1);
            break;
          default:
            abort();
            break;
        }
    }
    
    fINFORM(pf, "\n");

    if(!(pf2 = fopen(OUT, "rb"))){
      fINFORM(stderr, "Can't open file %s", OUT);
       return EXIT_FAILURE;
    }

    for(int i=0; i<number; i++)
        fwrite(&save[i], sizeof(int), number, pf);

    for(int i = 0; i<number; i++){
      if(save[i] % P == 0)
        INFORM("%i, ", save[i]);
    }
    printf("\n");

    for(int i = 0; i<number; i++){
      if(save[i] % P != 0)
        INFORM("%i, ", save[i]);
    }
    
    printf("\n");


    free(save);
    fclose(pf);

    return EXIT_SUCCESS;
}
