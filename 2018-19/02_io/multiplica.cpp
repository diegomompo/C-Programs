#include <stdio.h>
#include <stdlib.h>
#define TABLA 10

int main(){

    int numero;
    int i = 0;



    printf("Dime un número: ");
    scanf("%i", &numero);

    do{
    
     printf("%d * %d = %d\n", numero, i, numero * i);

     i++;
    }
    while(i <= TABLA );

    return EXIT_SUCCESS;
}
