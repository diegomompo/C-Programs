#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){

    double numero, raiz;

    // ENTRADA

    scanf("%lf", &numero);
    
    // CÁLCULOS

    if (numero >= 0){
    
        raiz = sqrt(numero);
        printf("%.2lf\n", raiz);

    }else{
    
        printf("La raíz de %.2lf es imaginaria\n", numero);
    }
    
    // SALIDA

    return EXIT_SUCCESS;
}
