#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 2
#define CUARTO 250000
#define MEDIO 500000
#define UNO 1000000
#define UNO_MEDIO 1500000
#define DOS 2000000

int Parte1_GER(){

    int i = 0;	

    while(i<VECES){
  
     fprintf(stderr, "\a");
     usleep(UNO_MEDIO);
     fprintf(stderr, "\a");
     usleep(MEDIO);
     fprintf(stderr, "\a");
     usleep(UNO);
     fprintf(stderr, "\a");
     usleep(UNO);
     fprintf(stderr, "\a");
     usleep(UNO);
     fprintf(stderr, "\a");
     usleep(UNO);
     fprintf(stderr, "\a");
     usleep(MEDIO);
     fprintf(stderr, "\a");
     usleep(MEDIO);
     fprintf(stderr, "\a");
     usleep(UNO);
     fprintf(stderr, "\a");
     usleep(UNO);
     fprintf(stderr, "\a");
     usleep(UNO);
     fprintf(stderr, "\a");
     usleep(UNO);
     fprintf(stderr, "\a");
     usleep(UNO);
     fprintf(stderr, "\a");
     usleep(UNO);
     fprintf(stderr, "\a");
     usleep(MEDIO);
     fprintf(stderr, "\a");
     usleep(MEDIO);
     fprintf(stderr, "\a");
     usleep(DOS);

     i++;

    }

    return EXIT_SUCCESS;
}
