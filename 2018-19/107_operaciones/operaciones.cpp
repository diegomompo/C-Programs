#include <stdio.h>
#include <stdlib.h>

#define N 2


int main(){

    double num[N];
    double sum, res, mul = 1, div;

    // ENTRADA
    
    for(int i=0; i<N; i++)
        scanf("%lf", &num[i]);

    // CALCULOS
    
    for(int i=0; i<N; i++){
        sum += num[i];
        mul *= num[i];
    }

    if(num[N-N] > num[N-1]){
    
        res = num[N-N] -  num[N-1];
        div = num[N-N] /  num[N-1];
    }
    else{
    
        res = num[N-1] -  num[N-N];
        div = num[N-1] /  num[N-N];

    }

    // SALIDA
    
    printf("%lf\n", sum);
    printf("%lf\n", res);
    printf("%lf\n", mul);
    printf("%lf\n", div);

    return EXIT_SUCCESS;
}
