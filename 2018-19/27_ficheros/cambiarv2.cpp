
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define fINFORMA fprintf
#define N 0x100

const char * program_name;

void print_usage(FILE *outstream, int exit_code){

    fprintf ( outstream, "\
Usage:      %s [options] \n\
\n\
          - Replace all the ocurrences of seek character (s) with\n\
          replace character (r) within file (f). \n\  
\n\
Options:\n\
        -f <Filename> \t Defaults to stdin \n\
        -r <character> \t Defaults to !.\n\
        -s <character>\t Defaults to i.\n\
        -h <help> \t Prints help on screen.\n\
Example:\n\
        %s -rd -sb -fa.txt\n\
", program_name, program_name);
    exit(exit_code);
}


char entrada(char buffer[N], int argc, char *argv[]){

    if(argc < 1)
        return EXIT_FAILURE;

    strcpy(buffer, argv[1]);
    
    return buffer[N];

}

int main(int argc, char *argv[]){

    FILE *res;
    char filename[N];
    int o;
    char r = '!'; // r: char to replace with
    char s = 'i'; // s: char to look for
    char f = '-';

    program_name = argv[0];

    while((o = getopt (argc, argv, "hf:r:s:")) != -1){
    
        switch(o){
            
            case 'h':
              print_usage (stdout, 0);
              break;
            case 'f':
              strncpy(filename, optarg, N);
              f = '\0';
            break;
            case 'r':
              r = *optarg;
              break;
            case 's':
              s = *optarg;
              break;
            case '?':
               if(optopt != 'f' && optopt != 'r' && optopt != 's')
                   fINFORMA(stderr, "Invalid option\n");
               else
                   fINFORMA(stderr, "Option argument missing\n");

               print_usage(stderr, 1);
               break;
            default:
               abort();
               break;
               
        }
    }

    FILE *pf = stdin;
    int c;

    if(!f)    
      if( !(pf = fopen (filename, "w+")) ){
         fINFORMA(stderr, "Couldn't find your %s\n", filename);
          print_usage (stderr, 2);
        }
    while ( (c = getc (pf) ) != EOF)
     printf("%c", c == s ? r : c);

    fclose(pf);
    
    return EXIT_SUCCESS;
}
