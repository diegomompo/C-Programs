#include<stdio.h>

#include<stdlib.h>

#include<string.h>

#include<time.h>

#include<math.h>

#include<ctype.h>



#define TRUE 1

#define FALSE 0

void imprimir(int oport, char *wordserr,char *word);

int found_word(char letra);

int menu(void);

int exist_word(char letra);

char *adivina(void)

{

    char *diccionario[]={"LEON","MANAGUA","CORINTO","PARIS","CANADA",

        "PERRO","GATO","CONEJO","ZORRO","CABALLO","PEZ",

        "MARTILLO","CLAVO","LAPIZ","MADERA","PEGA","CASA",

        "SILLA","MESA","COMEDOR","TABLA","PESAS","BARCO",

        "LANCHA","PESCA","ZAPATO","BICICLETA","AUTOBUS",

        "TRICICLO","MEXICO","LICENCIA","BEISBOL","FUTBOL",

        "BASKET","TENNIS","NATACION","MARATON","BALONMANO",

        "COMPUTADORA","POLO","HARDWARE","SOFTWARE","IMPRIMIR",

        "DISCO","FOTO","MARCO","GRANDEZA","PEQUE¥O","MOVIMIENTO",

        "CAZADOR","BALLENATO","NOCHE","DIA"};

    int num;

    srand( (unsigned)time( NULL ) );

    num=rand()%50;

    return(diccionario[num]);

}



char *cadena, palabra[10], letraserror[10];

main(){

    int i, cont=0,game_over=FALSE, oportunidades, acierto=FALSE, existe;

    int opc=0;

    char letra;

    system("cls");

    opc=menu();

    do{

        system("cls");

        switch(opc){

            case 1:

                cadena=adivina();

                strcpy(palabra,cadena);

                oportunidades=6;

                for(i=0;i<strlen(cadena);i++) palabra[i]='_';

                for(i=cont;i>=0;i--) letraserror[i]='\0';

                cont=0;

                do{

                    do{

                        fflush(stdin);

                        system("cls");

                        existe=acierto=FALSE;

                        imprimir(oportunidades,letraserror,palabra);

                        printf("\n\n\t Letra?: ");scanf("%c",&letra);

                        existe=exist_word(letra);

                    }while(existe);//fin do while

                    acierto=found_word(letra);

                    if(acierto){

                        if(strcmp(palabra, cadena)!=0){game_over=FALSE; continue;}

                        else game_over=TRUE;

                    }//fin if.

                    else {

                        letraserror[cont++]=letra;

                        --oportunidades;

                    }//fin else.

                }while(oportunidades>0&&game_over==FALSE);//fin do while.

                system("cls");

                if(game_over&&oportunidades>4){

                    imprimir(oportunidades,letraserror,palabra);

                    printf("\n\tEXCELENTE TRABAJO!");}//fin if.

                else if(game_over&&oportunidades>2){

                    imprimir(oportunidades,letraserror,palabra);

                    printf("\n\tMUY BUEN TRABAJO!");}//fin else if.

                else if(game_over){

                    imprimir(oportunidades,letraserror,palabra);

                    printf("\n\tHas ganado!!");}//fin else if.

                else {  printf("\n\tAHORCADO, JEJEJE!\n\n\t");

                    printf("\La palabra era...\n\n\t");

                    for(i=0;i<strlen(cadena);i++) printf(" %c",cadena[i]);

                }//fin else.

                game_over=FALSE;

                break;//fin case 1.

            case 2: exit(0);break;//fin case 2.

        }//fin switch.

        fflush(stdin);

        opc=menu();

    }while(opc==1);

}

void imprimir(int oport, char *wordserr,char *word)

{  int i;

    printf("\n\n\tJuego del Ahorcado\n\n\n\t");

    if(oport<6){

        printf("\n\n\tLetras erradas: ");

        for(i=0;i<strlen(wordserr);i++) printf(" %c",wordserr[i]);

    }

    printf("\n\n\t");

    for(i=0;i<strlen(cadena);i++) printf(" %c",word[i]);

    printf("\n\n\tOportunidades= %d\n",oport);

}

int menu(void)

{      int opcion;

    printf("\n\n\tJuego del Ahorcado\n\n\n"

            "\n\t1. Jugar\n"

            "\n\t2. Salir\n"

            "\n\t Elegir la opción: ");

    scanf("%d",&opcion);

    return(opcion);

}

int found_word(char letra)

{

    int acierto=FALSE,i;

    letra=toupper(letra);

    for(i=0;i<strlen(cadena);i++){

        if(letra==cadena[i]){

            palabra[i]=letra;

            acierto=TRUE;}

    }

    return(acierto);

}

int exist_word(char letra)

{

    int existe=FALSE,i;

    for(i=0;i<strlen(letraserror);i++){

        if(letra==letraserror[i]) {

            printf("\nYa la escribistes!.\n");

            existe=TRUE;

            system("pause");

        }

    }

    return(existe);

}
